import {useMutation, useQuery} from "react-query"
import api from "../api"
import {useRouter} from "next/router";

const getArticles = async () => {
    try {
        return await api.get("/article")
    } catch (err) {
        throw err
    }
}

export const getArticle = async (id) => {
    try {
        return await api.get(`/article/${id}`)
    } catch (err) {
        throw err
    }
}

const updateArticle = async (id, data) => {
    try {
        await api.put(`/article/${id}`, data)
    } catch (err) {
        throw err
    }
}

const deleteArticle = async (id) => {
    try {
        await api.delete(`/article/${id}`)
    } catch (err) {
        throw err
    }
}

const createArticle = async (data) => {
    try {
        await api.post(`/article`, data)
    } catch (err) {
        throw err
    }
}

const uploadFile = async (data) => {
    try {
        return await api.post(`/image-upload`, data)
    } catch (err) {
        throw err
    }
}

export const useArticles = () => {
    return useQuery(["rates"], () => getArticles())
}

export const useArticle = (pid) => {
    const router = useRouter()
    const id = pid ?? router?.query.id
    return useQuery(["rate", id], () => getArticle(id), {
        enabled: !!id,
    })
}

export const useUpdateArticle = ({onSuccess, onError}) => {
    const router = useRouter()
    const id =  router?.query.id
    return useMutation(data => updateArticle(id, data), {onSuccess, onError})
}

export const useDeleteArticle = ({onSuccess, onError}) => {
    const router = useRouter()
    const id =  router?.query.id
    return useMutation(() => deleteArticle(id), {
        onSuccess,
        onError,
    })
}

export const useCreateArticle = ({onSuccess, onError}) => {
    return useMutation(
        data => createArticle(data),
        {
            onSuccess,
            onError,
        }
    )
}

export const useUploadFile = ({onSuccess, onError}) => {
    return useMutation(
        data => uploadFile(data),
        {
            onSuccess,
            onError,
        }
    )
}
