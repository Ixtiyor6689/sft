import React from 'react';
import styles from "./header.module.scss"
import Link from "next/link";
import {AiOutlineMenu} from "react-icons/ai"
import {useRouter} from "next/router";

function Header({isOpen, setIsOpen}) {
    const navigate = useRouter()
    return (
        <div className={styles.header}>
            <div className={`${styles.content} pd`}>
                <div className={styles.logo}>
                        <Link href="/">
                            <img src="/logo.png" alt="logo" className={styles.img}/>
                        </Link>
                </div>
                <div className={styles.pages}>
                    <Link href="/about" className={styles.lk}>
                        <div className={styles.link}>About</div>
                    </Link>
                    <Link href="/services" className={styles.lk}>
                        <div className={styles.link}>Services</div>
                    </Link>
                    <Link href="/blogs" className={styles.lk}>
                        <div className={styles.link}>Blogs</div>
                    </Link>
                    <Link href="/team" className={styles.lk}>
                        <div className={styles.link}>Team</div>
                    </Link>
                    <Link href="/contact" className={styles.lk}>
                        <div className={styles.link}>Contact</div>
                    </Link>
                    <Link href="/help" className={styles.lk}>
                        <div className={styles.link}>Help</div>
                    </Link>
                </div>
                <div className={styles.quote} onClick={() => navigate.push("/quote")}>
                    <button className={styles.glowOnHover} type="button">GET A QUOTE</button>
                </div>
                <div className={styles.menu} onClick={() => setIsOpen(!isOpen)}>
                    <AiOutlineMenu className={styles.icon}/>
                </div>
            </div>
        </div>
    );
}

export default Header;
