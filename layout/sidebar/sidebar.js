import React from 'react';
import Link from "next/link"
import styles from "./sidebar.module.scss"
import {useRouter} from "next/router";

function Sidebar({isOpen, setIsOpen}) {
    const navigate = useRouter()

    const handleNavigate = (path) => {
        setIsOpen(false)
        navigate.push(path)
    }

    const handlePath = () => {
        navigate.push("/quote");
        setIsOpen(false)
    }

    return (
        <div className={`${styles.sidebar} ${isOpen ? styles.active : ""}`}>
            <div className={styles.sio}>
                <div className={styles.lg}>
                    <Link href="/" onClick={() => handleNavigate("/")}>
                        <img src="/logo.png" alt="logo"/>
                    </Link>
                </div>
                <ul>
                    <li onClick={() => handleNavigate("/about")}>
                        About
                    </li>
                    <li onClick={() => handleNavigate("/services")}>
                        Services
                    </li>
                    <li onClick={() => handleNavigate("/blogs")}>
                        Blogs
                    </li>
                    <li onClick={() => handleNavigate("/team")}>
                        Team
                    </li>
                    <li onClick={() => handleNavigate("/contact")}>
                        Contact
                    </li>
                    <li onClick={() => handleNavigate("/help")}>
                        Help
                    </li>
                </ul>
                <div className={styles.bt}>
                    <button className={styles.glowOnHover} type="button" onClick={() => handlePath()}>GET A QUOTE
                    </button>
                </div>
                <div className={styles.tel}>
                    <a href="tel:315-314-4337">315-314-4337</a>
                </div>
                <ul className={styles.ntr}>
                    <li onClick={() => handleNavigate("https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC")}>
                        Our license
                    </li>
                    <li onClick={() => handleNavigate("/")}>
                        Terms & Conditions
                    </li>
                    <li onClick={() => handleNavigate("/")}>
                        Contact us
                    </li>
                </ul>
            </div>
        </div>
    );
}

export default Sidebar;
