import React from 'react';
import {GoLocation} from "react-icons/go"
import {AiFillPhone, AiFillStar} from "react-icons/ai"
import {FaFax} from "react-icons/fa"
import {HiOutlineMail} from "react-icons/hi"
import {BsFillCalendarFill} from "react-icons/bs"
import styles from "./footer.module.scss"

function Footer(props) {
    return (
        <div className={styles.footer}>
            <div className={`${styles.inner} pd`}>
                <div className={styles.col}>
                    <img src="/yellow-logo.png"
                         alt="footerImg"/>
                    <div className={styles.txfr}>
                        Safeeds Transport Inc is accredited and highly rated with the BBB. We offer REAL Discount car
                        shipping
                        rates
                        and
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        Comprehensive Auto Transport Protection with the AFta PLAN. Don't ship your car before you give
                        us a
                        call. We want to earn your Trust. MC # : 01462168 USDOT # : 3938800
                    </div>
                </div>
                <div className={styles.col}>
                    <div className={styles.title}>
                        CONTACT US
                    </div>
                    <div className={styles.list}>
                        <div className={styles.item}>
                            <GoLocation className={styles.icon}/>
                            <div className={styles.tc}>
                                1201 Avenue K 3b,
                                Brooklyn, NY 11230,
                                United States
                            </div>
                        </div>
                        <div className={styles.item}>
                            <AiFillPhone className={styles.icon}/>
                            <div className={styles.tc}>
                                <a href="tel:3153144337">Phone: (315)-314-4337</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <FaFax className={styles.icon}/>
                            <div className={styles.tc}>
                                <a href="tel:3153144337">Fax: (315)-314-4337</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <HiOutlineMail className={styles.icon}/>
                            <div className={styles.tc}>
                                <a href="mailto:info@safeeds.us">Email: info@safeeds.us</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <BsFillCalendarFill className="icon"/>
                            <div className={styles.tc}>
                                Mon - Fri: 8am - 7pm EST
                                <br/>
                                Sat - Sun: 9am - 5pm
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.col}>
                    <div className={styles.title}>
                        REVEWS
                    </div>
                    <div className={styles.list}>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <div>
                                    TRANSPORT REVIEWS
                                </div>
                                <div className={styles.stars}>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                </div>
                                <div className={styles.nums}>
                                    (225 Reviews)
                                </div>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <div>
                                    FACEBOOK
                                </div>
                                <div className={styles.stars}>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                </div>
                                <div className={styles.nums}>
                                    (170 Reviews)
                                </div>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <div>
                                    GOOGLE
                                </div>
                                <div className={styles.stars}>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                </div>
                                <div className={styles.nums}>
                                    (284 Reviews)
                                </div>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <div>
                                    BBB
                                </div>
                                <div className={styles.stars}>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                    <AiFillStar className={styles.star}/>
                                </div>
                                <div className={styles.nums}>
                                    (136 Reviews)
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={styles.col}>
                    <div className={styles.title}>
                        QUICK LINKS
                    </div>
                    <div className={styles.list}>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a href="https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC">Our
                                    Licence</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a href="/terms-and-conditions">Terms & Conditions</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a>Condition Report & Checklist</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a href="">Car Shipping Analytics</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a href="">How Auto Transport Works</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a href="">How We Determine Your Quote</a>
                            </div>
                        </div>
                        <div className={styles.item}>
                            <div className={styles.tc}>
                                <a href="">The Safeeds Guarantee</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className={`${styles.line} pd`}/>
            <div className={`${styles.term} pd`}>
                <div className={styles.chl}>
                    Website Terms
                </div>
                <div className={styles.chl}>
                    Privacy Policy
                </div>
                <div className={styles.chl}>
                    Accessibility Statement
                </div>
                <div className={styles.chl}>
                    CA Transparency in Supply Chains Act
                </div>
                <div className={styles.chl}>
                    Supplier Code of Conduct
                </div>
                <div className={styles.chl}>
                    Marketing to Children
                </div>
                <div className={styles.chl}>
                    Do Not Sell My Information
                </div>
            </div>
            <div className={`${styles.rg} pd`}>
                &copy;2022 Safeeds Transport Inc, LLC. All Rights Reserved
            </div>
        </div>
    );
}

export default Footer;
