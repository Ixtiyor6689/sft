import React from 'react';
import styles from "./aSec1.module.scss"

function ASec1(props) {
    return (
        <div className={styles.aSec1}>
            <div style={{
                backgroundImage:
                    `url("./about.webp")`
            }}
                 className={styles.banner}
            >
                <div className={styles.bg}/>
                <div className={styles.content}>
                    <h1 className={styles.t1}>
                        About Us
                    </h1>
                    <p className={styles.t2}>
                        Safeeds Transport Inc has grown to one of the largest auto transport companies in the country
                        in less than five years!
                    </p>
                </div>
            </div>
        </div>
    );
}

export default ASec1;
