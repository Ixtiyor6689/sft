import styles from "./reviews.module.scss"
import Slider from "react-slick";
import {AiFillStar} from "react-icons/ai"
import {useSatisfieds} from "hooks";
import {Empty, Spin} from "antd";
import {MainApi} from "../../../api";

function TeamReviews({broker}) {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    const {data: satisfiedUsers, isLoading} = useSatisfieds()
    const d = satisfiedUsers?.data?.data?.filter(p => p?.broker?.toString().toLowerCase() === broker.toString().toLowerCase())

    return (
        <div className={`${styles.reviewTeams}`}>
            <Spin spinning={isLoading}>
                {
                    !!d?.length ?
                        <div className={styles.slider}>
                            <Slider {...settings}>
                                {
                                    d.map((i, k) => {
                                        return (
                                            <div className={styles.cardWr} key={k}>
                                                <div className={styles.card}>
                                                    <div className={styles.top}>
                                                        <div className={styles.abs}>
                                                            <img src={`${MainApi}/${i?.icon}`} alt="someImg"
                                                                 className={styles.img}/>
                                                        </div>
                                                        <div className={styles.name}>
                                                            {i?.name}
                                                        </div>
                                                        <div className={styles.st}>
                                                            {
                                                                Array.from(Array(i.star).keys())?.map((m, index) => {
                                                                    return (
                                                                        <div key={index}>
                                                                            <AiFillStar className={styles.star}/>
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                    </div>
                                                    <div className={styles.bottom}
                                                         dangerouslySetInnerHTML={{__html: i?.commment}}/>
                                                </div>
                                            </div>
                                        )
                                    })
                                }
                            </Slider>
                        </div>
                        :
                        <Empty/>
                }
            </Spin>
        </div>
    );
}

export default TeamReviews;
