import React from 'react';
import styles from "./cards.module.scss"
import {AiOutlineMail,AiOutlinePhone} from "react-icons/ai"
import Button from "../../elements/button/button";
import {data} from "../data";
import {useRouter} from "next/router";

function Cards(props) {
    const navigate = useRouter()
    return (
        <div className={`${styles.tcds} pd`}>
            <h1 className={styles.title}>
                Our Team
            </h1>
            <div className={styles.ty}>
                <div className={styles.tc}>
                    All Members <span>52</span>
                </div>
                <div className={styles.line}/>
            </div>
            <div className={styles.rw}>
                {
                    data?.map((i, k) => {
                        return (
                            <div key={k} className={styles.bxs}>
                                <div style={{backgroundImage: `url("${i.img}")`}} className={styles.img}/>
                                <div className={styles.tcw}>
                                    <div className={styles.txt1}>
                                        <h6 className={styles.name}>
                                            {i.name}
                                        </h6>
                                    </div>
                                </div>
                                <div className={styles.trew}>
                                    <div className={styles.txt223}>
                                        <div className={styles.iwr}>
                                            <AiOutlineMail className={styles.icon}/>&nbsp;Email:
                                        </div>
                                        <div className={styles.iwr1}>
                                            {i?.email}
                                        </div>
                                    </div>
                                    <div className={styles.txt223}>
                                        <div className={styles.iwr}>
                                            <AiOutlinePhone className={styles.icon1}/>&nbsp;Phone:
                                        </div>
                                        <div className={styles.iwr1}>
                                            {i?.phone}
                                        </div>
                                    </div>
                                </div>
                                <div className={styles.btr}>
                                    <Button title="View profile" size="18px" onClick={() => navigate.push(`/team/${i?.name.replace(/\s/g, '-')}`)}/>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    );
}

export default Cards;
