import React from 'react';
import styles from "./sc1.module.scss"
import {BsFillPersonFill} from "react-icons/bs"
import {useArticles} from "../../../hooks";
import {MainApi} from "../../../api";
import {Spin} from "antd";
import {useRouter} from "next/router";

function Sc1(props) {
    const navigate = useRouter()

    const {data: articles, isLoading} = useArticles()

    const handleNavigate = (i) => {
        let title = i?.title.toString()
        if (title?.at(-1) === "?")
            title = title.slice(0, -1)
        navigate.push(`/blogs/${title?.replace(/\s/g, '-')}`)
    }

    return (
        <Spin spinning={isLoading}>
            <div className={`${styles.wr} pd`}>
                {
                    articles?.data?.data?.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())?.map((i, k) => {
                        return (
                            <div key={k}
                                 className={styles.cd}
                                 onClick={() => handleNavigate(i)}
                            >
                                <div style={{backgroundImage: `url("${MainApi}/${i?.image}")`}} className={styles.smg}/>
                                <div className={styles.cnt}>
                                    <div className={styles.t1}>
                                        {i?.title}
                                    </div>
                                    <div className={styles.tx} dangerouslySetInnerHTML={{__html: i?.body}}/>
                                    <div className={styles.si}>
                                        <div className={styles.ic1}>
                                            <BsFillPersonFill className={styles.icon}/>
                                        </div>
                                        <div className={styles.tr}>
                                            <div className={styles.pr}>{i?.name}</div>
                                            <div className={styles.date}>
                                                {
                                                    `${new Date(i?.createdAt).getUTCFullYear()}-${new Date(i?.createdAt).getMonth() + 1 > 9 ?
                                                        new Date(i?.createdAt).getMonth() + 1 :
                                                        `0${new Date(i?.createdAt).getMonth() + 1}`
                                                    }-${new Date(i?.createdAt).getDate() > 9 ?
                                                        new Date(i?.createdAt).getDate() :
                                                        `0${new Date(i?.createdAt).getDate()}`
                                                    }
                                        `
                                                }
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </Spin>
    );
}

export default Sc1;
