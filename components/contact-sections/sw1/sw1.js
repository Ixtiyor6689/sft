import React from 'react';
import styles from "./sw1.module.scss"
import {BsTelephone} from "react-icons/bs"
import {GoLocation} from "react-icons/go"
import {TiMessage} from "react-icons/ti"

function Sw1(props) {
    return (
        <div className={`${styles.sw1} pd`}>
            <h1 className={styles.title}>
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                We're here to help
            </h1>
            <div className={styles.rws}>
                <div className={styles.bx}>
                    <div className={styles.ic}>
                        <BsTelephone/>
                    </div>
                    <div className={styles.call}>
                        Give Us A Call
                    </div>
                    <div className={styles.tel}>
                        Local: <span>315-314-4337</span>
                    </div>
                </div>
                <div className={styles.bx}>
                    <div className={styles.ic}>
                        <TiMessage/>
                    </div>
                    <div className={styles.call}>
                        Email Us
                    </div>
                    <div className={styles.tel}>
                        <a href="mailto:info@safeeds.us">info@safeeds.us</a>
                    </div>
                </div>
                <div className={styles.bx}>
                    <div className={styles.ic}>
                        <GoLocation/>
                    </div>
                    <div className={styles.call}>
                        Our Address
                    </div>
                    <div className={styles.tel}>
                        Our team is worldwide and our physical office is located at:
                    </div>
                    <div className={styles.ntel}>
                        1201 Avenue K 3b
                    </div>
                    <div className={styles.ntel}>
                        Brooklyn, NY 11230,
                        United States
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Sw1;
