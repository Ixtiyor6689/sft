import React from 'react';
import styles from "./acmem.module.scss"
import {BsFillPersonFill} from "react-icons/bs"


function Acmem(props) {
    const data = [
        {
            name: "Jake Scott",
            action: "Posted a new blog",
            date: "1 days ago",
            img: "/account.png"
        },
        {
            name: "Richard Kane",
            action: "Updated their profile",
            date: "4 days ago",
            img: "/account.png"
        },
        {
            name: "David Johnson",
            action: "Updated their profile",
            date: "2 days ago",
            img: "/account.png"
        },
        {
            name: "Frank Lannister",
            action: "Updated their profile",
            date: "2 days ago",
            img: "/account.png"
        },
    ];
    const people = [
        {
            name: "Michael Steven",
            img: ""
        },
        {
            name: "Richard Kane",
            img: ""
        },
        {
            name: "David Johnson",
            img: ""
        },
        {
            name: "Frank Lannister",
            img: ""
        },
        {
            name: "Jake Scott",
            img: ""
        },
        {
            name: "Tony Davis",
            img: ""
        },
        {
            name: "Trever Noah",
            img: ""
        },
        {
            name: "Rick Novak",
            img: ""
        },
        {
            name: "Ronald Barr",
            img: ""
        },
        {
            name: "Roger Lum",
            img: ""
        },
        {
            name: "Jeff Johnson",
            img: ""
        },
        {
            name: "Lena Smith",
            img: ""
        },
        {
            name: "Olivia Perry",
            img: ""
        },
        {
            name: "Melvin Forbis",
            img: ""
        },
        {
            name: "Obmar Wood",
            img: ""
        },
    ]
    return (
        <div className={`${styles.acmem} pd`}>
            <div className={styles.c1}>
                <div className={styles.ft}>
                    <div className={styles.lf}>
                        Activity
                    </div>
                    <div className={styles.rg}>
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        ALL ACTIVITY >
                    </div>
                </div>
                {
                    data?.map((i, k) => {
                        return (
                            <div key={k} className={styles.rw}>
                                <div style={{backgroundImage: `url(${i.img})`}} className={styles.img}/>
                                <div className={styles.txs}>
                                    <div className={styles.na}>
                                        {i.name} <span>{i.action}</span>
                                    </div>
                                    <div className={styles.date}>
                                        {i.date}
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
                <div className={styles.card}>
                    <img
                        className={styles.mim}
                        src="./ship.webp"
                        alt="img"
                    />
                    <p className={styles.t1}>How to Ship Your Car When Moving Long Distance</p>
                    <p className={styles.t2}>2021 saw 8.4% of Americans up sticks and head for pastures new. Are you planning a big move
                        sometime soon? Maybe you’re a…</p>
                </div>
            </div>
            <div className={styles.c1}>
                <div className={styles.ft}>
                    <div className={styles.lf}>
                        Team Members
                    </div>
                    <div className={styles.rg}>
                        {/* eslint-disable-next-line react/no-unescaped-entities */}
                        ALL STAFF >
                    </div>
                </div>
                {
                    people?.map((i, k) => {
                        return (
                            <div key={k} className={styles.rw}>
                                <div className={styles.icImg}>
                                    <BsFillPersonFill/>
                                </div>
                                <div className={styles.tx1}>
                                    {i.name}
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    );
}

export default Acmem;
