import React from 'react';
import styles from "./s1.module.scss"
import Card from "../card/card";

function S1(props) {
    return (
        <div className={styles.servicesS1}>
            <div className={styles.f1}>
                <div className={styles.left}>
                    <h3 className={styles.tit}>
                        EVERY SERVICE WE OFFER INCLUDES
                    </h3>
                    <h1 className={styles.bt}>
                        Door-To-Door Service, 100% Bumper-To-Bumper Insurance
                    </h1>
                    <p className={styles.tx1}>
                        When you go with Safeeds Transport Inc to ship your vehicle, you can rest assured that the carrier assigned to
                        move your car is fully insured and licensed with the&nbsp;<span>FMCSA (Federal Motor Carrier and Saftey
                        Administration).
                        </span>
                    </p>
                    <p className={styles.tx2}>
                        Your vehicle is covered by the carrier’s insurance policy that is doing the shipping. Depending
                        on the type and size of the truck/trailer that is shipping your car, the insurance policy can
                        cover from $100,000 to $1,000,000 of damage.
                    </p>
                    <div className={styles.tx3}>
                        Ready to get started?
                    </div>
                    <div className={styles.cr}>
                        <Card/>
                    </div>
                </div>
                <div className={styles.right1}>
                    <div className={styles.rimg}/>
                </div>
            </div>
        </div>
    );
}

export default S1;
