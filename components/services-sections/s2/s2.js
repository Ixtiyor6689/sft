import React from 'react';
import styles from "./s2.module.scss"
import { useMediaQuery } from 'react-responsive'

function S2({title, text, picture, order}) {

    const is300 = useMediaQuery({query: "(max-width: 1300px)"})

    return (
        <div className={styles.cta}>
            <div className={`${styles.bx} ${is300 ? "" : "pd"}  ${order ? styles.str : styles.opp}`}>
                <div className={styles.l1}>
                    <div className={styles.t1}>
                        {title}
                    </div>
                    <div className={styles.tx}>
                        {text}
                    </div>

                </div>
                <div
                    className={styles.r1}
                    style={{background: `url(${picture})`, backgroundPosition: "center",backgroundSize:"cover"}}
                />
            </div>
        </div>
    );
}

export default S2;
