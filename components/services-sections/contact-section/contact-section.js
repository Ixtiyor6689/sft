import React from 'react';
import styles from "./contact-section.module.scss"
import img from "../../../public/disp.jpg"
import {useRouter} from "next/router";

function ContactSection(props) {
    const navigate = useRouter()

    return (
        <div className={styles.contactSection}>
            <div className={styles.bg}/>
            <div className={styles.content}>
                <div className={styles.text}>
                    Call us 24/7 at 315-314-4337
                </div>
                <div className={styles.btiy} onClick={() => navigate.push("/quote")} style={{cursor:"pointer"}}>
                    REQUEST A QUOTE
                </div>
            </div>
            <div className={styles.img}>
                <img src="/disp.jpg" alt="rq"/>
            </div>
        </div>
    );
}

export default ContactSection;
