import React from 'react';
import styles from "./vd.module.scss"
import dynamic from "next/dynamic";

const ReactPlayer = dynamic(() => import("react-player"), { ssr: false });

function Vd(props) {
    return (
        <div className={`${styles.vd} pd`}>
            <ReactPlayer
                url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
                className={styles.player}
            />
        </div>
    );
}

export default Vd;
