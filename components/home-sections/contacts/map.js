import React from 'react'
import {GoogleMap, MarkerF, useJsApiLoader} from '@react-google-maps/api';

const containerStyle = {
    width: '100%',
    height: '80vh'
};

const center = {
    lat: 40.62284628985301,
    lng: -73.96335934418308
};

const markers = [
    {
        id: 1,
        name: "Nyu York",
        position: {
            lat: 40.62284628985301,
            lng: -73.96335934418308
        }
    },
]

function MyComponent() {
    const {isLoaded} = useJsApiLoader({
        id: 'google-map-script',
        googleMapsApiKey: "AIzaSyCB1vQhgArG3pHSL4T8_UB_E5Vghji8BBE"
    })

    const handleOnLoad = (map) => {
        map.setZoom(11)
    };

    return isLoaded ? (
        <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            options={{zoom: 11}}
            onLoad={handleOnLoad}
        >
            { /* Child components, such as markers, info windows, etc. */}
            <>
                {markers.map(({id, position}) => (
                    <MarkerF
                        key={id}
                        position={position}
                    />
                ))}
            </>
        </GoogleMap>
    ) : <></>
}

export default React.memo(MyComponent)
