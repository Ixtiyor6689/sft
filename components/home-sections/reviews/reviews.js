import React from 'react';
import styles from "./reviews.module.scss"
import Slider from "react-slick";
import {AiFillStar} from "react-icons/ai"
import {useSatisfieds} from "hooks";
import {Spin} from "antd";
import {MainApi} from "../../../api";

function Reviews(props) {

    const settings = {
        infinite: true,
        speed: 5000,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 0,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                }
            }
        ]
    };

    const {data: satisfiedUsers, isLoading} = useSatisfieds()

    return (
        <div className={`${styles.reviews} pd`}>
            <div className={styles.title}>
                Reviews From Our Satisfied Customers
            </div>
            <Spin spinning={isLoading}>
                <div className={styles.slider}>
                    <Slider {...settings}>
                        {
                            satisfiedUsers?.data?.data?.map((i, k) => {
                                return (
                                    <div className={styles.cardWr} key={k}>
                                        <div className={styles.card}>
                                            <div className={styles.top}>
                                                <div className={styles.abs}>
                                                    <img src={`${MainApi}/${i?.icon}`} alt="someImg" className={styles.img}/>
                                                </div>
                                                <div className={styles.name}>
                                                    {i?.name}
                                                </div>
                                                <div className={styles.st}>
                                                    {
                                                        Array.from(Array(i.star).keys())?.map((m, index) => {
                                                            return (
                                                                <div key={index}>
                                                                    <AiFillStar className={styles.star}/>
                                                                </div>
                                                            )
                                                        })
                                                    }
                                                </div>
                                            </div>
                                            <div className={styles.bottom} dangerouslySetInnerHTML={{__html: i?.commment}}/>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </Slider>
                </div>
            </Spin>
        </div>
    );
}

export default Reviews;
