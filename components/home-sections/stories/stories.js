import React from 'react';
import styles from "./stories.module.scss"
import {useArticles} from "../../../hooks";
import {Spin} from "antd";
import {MainApi} from "../../../api";
import {useRouter} from "next/router";

function Stories(props) {
    const navigate = useRouter()

    const {data: articles, isLoading} = useArticles()

    return (
        <div className={`${styles.stories} pd`}>
            <div className={styles.title}>
                Latest stories for you
            </div>
            <Spin spinning={isLoading}>
                <div className={styles.wr}>
                    {
                        articles?.data?.data?.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())?.slice(0,3)?.map((i, k) => {
                            return (
                                <div className={styles.latest} key={k} onClick={() => navigate.push(`/blogs/${i?.title?.replace(/\s/g, '-')}`)}>
                                    <div style={{backgroundImage: `url(${MainApi}/${i?.image})`}} className={styles.img}/>
                                    <div className={styles.t1}>
                                        {i?.title}
                                    </div>
                                    <div className={styles.txMain} dangerouslySetInnerHTML={{__html: i?.body}}/>
                                    <div className={styles.date}>Posted on:&nbsp;
                                        {
                                            `${new Date(i?.createdAt).getUTCFullYear()}-${new Date(i?.createdAt).getMonth() + 1 > 9 ?
                                                new Date(i?.createdAt).getMonth() + 1 :
                                                `0${new Date(i?.createdAt).getMonth() + 1}`
                                            }-${new Date(i?.createdAt).getDate() > 9 ?
                                                new Date(i?.createdAt).getDate() :
                                                `0${new Date(i?.createdAt).getDate()}`
                                            }
                                        `
                                        }
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </Spin>
        </div>
    );
}

export default Stories;
