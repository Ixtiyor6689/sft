import React from 'react';
import styles from "./partners.module.scss"
import Slider from "react-slick";

function Partners(props) {

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    };

    const data = [
        {
            img: "/partnerImgs/slide2.png",
            link: "https://www.mymovingreviews.com/car-movers/safeeds-transport-inc-33673"
        },
        {
            img: "/partnerImgs/slide3.png",
            link: "https://www.copart.com"
        },
        {
            img: "/partnerImgs/slide4.png",
            link: "https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC"
        },
        {
            img: "/partnerImgs/slide5.png",
            link: "https://www.transportreviews.com/Company/Safeeds-Transport-Inc"
        },
        {
            img: "/partnerImgs/slide7.png",
            link: "https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC"
        },
        {
            img: "/partnerImgs/bbb.png",
            link: "https://www.bbb.org/us/ny/brooklyn/profile/trucking-transportation-brokers/safeeds-transport-inc-0121-87157757"
        }
    ];

    const handleNavigate = (link) => {
        window.location.href = link
    }

    return (
        <div className={`${styles.partners} pd`}>
            <div className={styles.partner}>
                OUR PARTNERS:
            </div>
            <div className="slider">
                <Slider {...settings}>
                    {
                        data?.map((i, k) => {
                            return (
                                <div key={k} className="mlr20" onClick={() => handleNavigate(i?.link)}>
                                    <img src={i.img} alt={`img${k}`} className="bgfd"/>
                                </div>
                            )
                        })
                    }
                </Slider>
            </div>
        </div>
    );
}

export default Partners;
