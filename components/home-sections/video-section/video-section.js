import React from 'react';
import styles from "./video-section.module.scss"
import {IoDesktopOutline} from "react-icons/io5"
import {FaCar} from "react-icons/fa"
import {TiTick} from "react-icons/ti"
import dynamic from 'next/dynamic'

const ReactPlayer = dynamic(() => import("react-player"), { ssr: false });

function VideoSection(props) {

    return (
        <div className={styles.videoSection}>
            <div className="pd">
                <div className={styles.t1}>
                    How do I book with an auto transport company?
                </div>
                <div className={styles.t2}>
                    {/* eslint-disable-next-line react/no-unescaped-entities */}
                    Safeeds Transport Inc's guide to quick, easy and safe auto shipping
                </div>
                <div className={styles.vd}>
                    <div className={styles.vid}>
                        <ReactPlayer
                            url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
                            className={styles.player}
                        />
                    </div>
                    <div className={styles.box}>
                        <div className={styles.mt}>
                            Our auto transporter steps:
                        </div>
                        <div className={styles.steps}>
                            <div className={styles.circle}>
                                <IoDesktopOutline className={styles.ic}/>
                            </div>
                            <div className={styles.st}>
                                <div className={styles.tit1}>Step 1</div>
                                <div className={styles.tit2}>Quote and book your order</div>
                            </div>
                        </div>
                        <div className={styles.tx}>
                            Ready to experience something different? Safeeds Transport Inc is an online car shipping
                            marketplace that
                            puts you in complete control.
                        </div>
                        <div className={styles.steps}>
                            <div className={styles.circle}>
                                <FaCar className={styles.ic}/>
                            </div>
                            <div className={styles.st}>
                                <div className={styles.tit1}>Step 2</div>
                                <div className={styles.tit2}>We pick up your vehicle</div>
                            </div>
                        </div>
                        <div className={styles.tx}>
                            Upon booking, you will instantly receive an Order Confirmation email. Look for that in your
                            email inbox or drag it over from another folder.
                        </div>
                        <div className={styles.steps}>
                            <div className={styles.circle}>
                                <TiTick className={styles.ic}/>
                            </div>
                            <div className={styles.st}>
                                <div className={styles.tit1}>Step 3</div>
                                <div className={styles.tit2}>Receive your vehicle</div>
                            </div>
                        </div>
                        <div className={styles.tx}>
                            The auto transporter, or carrier, is once again not supposed to just show up unannounced.
                            The driver ideally should call the day before to alert your destination contact of his
                            pending arrival, and will give a rough estimation of what time of day to expect.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default VideoSection;
