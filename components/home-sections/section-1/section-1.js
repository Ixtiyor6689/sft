import React, {useEffect, useState} from 'react';
import styles from "./section-1.module.scss"
import {Button, Form, Radio} from 'antd';
import SelectOption from "../../elements/select/select";
import {fetchSearchFields} from "../../../utils/functions/fetchOptions";
import {useDispatch} from "react-redux";
import {setQuoteLocations} from "../../../redux";
import { useRouter } from 'next/router'


function Section1(props) {
    const dispatch = useDispatch()
    const navigate = useRouter()
    const [form] = Form.useForm();
    const [value, setValue] = useState("Open");
    const [p, setP] = useState("")
    const [d, setD] = useState("")

    const onFinish = (values) => {
        dispatch(setQuoteLocations({
            ...values,
            type:value
        }))
        navigate.push("/quote")
    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };


    const onChange = (e) => {
        setValue(e.target.value);
    };

    useEffect(() => {
        if (!!p) {
            form.setFieldValue("pickup", p)
        }
        if (!!d) {
            form.setFieldValue("delivery", d)
        }
    }, [p, d, form])

    return (
        <div className={styles.section1}>
            <div
                className={styles.sWr}
                style={{backgroundImage: `url("/covers.jpg")`}}>
                <div className={styles.bgMaker}>
                    <h1 className={styles.boldtext}>
                        The leading transportation company <br/> all over the states
                    </h1>
                    <p className={styles.sText}>
                        Safeeds Transport Inc provides the best transport experience with upfront,
                        transparent <br/> pricing
                        backed by our Price Lock Promise.
                    </p>
                </div>
            </div>
            <div className={styles.card}>
                <div className={styles.title}>
                    Request a quote online in under two minutes!
                </div>
                <div className={styles.line}/>
                <div className={styles.tr}>
                    Transport My Vehicle From...
                </div>
                <Form
                    name="basic"
                    labelCol={{
                        span: 24,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    layout="vertical"
                    form={form}
                >
                    <div className={styles.inputs}>
                        <div className={styles.col}>
                            <Form.Item
                                label="Pickup location"
                                name="pickup"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Pickup location!',
                                    },
                                ]}
                            >
                                <SelectOption
                                    fetchOptions={search => fetchSearchFields(search, "zipcode")}
                                    placeholder="ENTER ZIP CODE OR CITY"
                                    setP={setP}
                                    type="pickup"
                                />
                            </Form.Item>
                        </div>
                        <div className={styles.col}>
                            <Form.Item
                                label="Delivery location"
                                name="delivery"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Please input your Delivery location!',
                                    },
                                ]}
                            >
                                <SelectOption
                                    fetchOptions={search => fetchSearchFields(search, "zipcode")}
                                    placeholder="ENTER ZIP CODE OR CITY"
                                    setD={setD}
                                    type="delivery"
                                />
                            </Form.Item>
                        </div>

                        <Form.Item
                        >
                            <Button className={styles.button}
                                    type="primary"
                                    htmlType="submit"
                            >
                                Submit
                            </Button>
                        </Form.Item>
                    </div>
                </Form>
                <div className={styles.txt}>
                    <div className={styles.t1}>
                        Select <span>Transport Type</span>
                    </div>
                    <div className={styles.rd}>
                        <Radio.Group onChange={onChange} value={value}>
                            <Radio value="Open">Open</Radio>
                            <Radio value="Enclosed">Enclosed</Radio>
                        </Radio.Group>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Section1;
