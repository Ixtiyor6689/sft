import React from 'react';
import styles from "./call.module.scss"

function Call(props) {
    return (
        <div className={styles.lock}>
            <div className={styles.tit}>
                Lock in Your Price Today
            </div>
            <div className={styles.card}>
                <div className={styles.f1}>
                    <div className={styles.text}>Talk to an auto <br/> transport <br/> expert now!</div>
                    <div className={styles.img}>
                        <img
                            src="./callOptimized.jpg"
                            alt="img"/>
                    </div>
                </div>
                <div>
                    <a href="tel:3153144337" className={styles.f2}>CALL NOW: 315-314-4337</a>
                </div>
            </div>
        </div>
    );
}

export default Call;
