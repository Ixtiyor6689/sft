import React from 'react';
import styles from "./banner.module.scss"

function Banner(props) {

    return (
        <div style={{
            backgroundImage: `url("./bd.jpg")`
        }}
             className={styles.bannerHelp}
        >
            <div className={styles.drMaker}>
                <div className="pd">
                    <h1 className={styles.tr}>
                        Terms & conditions
                    </h1>
                    <div className={styles.mt}>
                        Safeeds Transport Inc
                    </div>
                    <a href="./terms_and_conditions.pdf" download className={styles.bt}>
                        Download as PDF
                    </a>
                </div>
            </div>
        </div>
    );
}

export default Banner;
