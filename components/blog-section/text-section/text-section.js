import React from 'react';
import Card from "../../services-sections/card/card";
import styles from "./text-section.module.scss"
import {MainApi} from "../../../api";
import {useArticles} from "../../../hooks";
import {Spin} from "antd";
import {useRouter} from "next/router";

function TextSection({article}) {
    const navigate = useRouter()
    const data = article?.data?.data

    const arr = data?.body?.split(/\./);

    arr?.shift();

    let rest = arr?.join('. ');

    const {data: articles, isLoading} = useArticles()

    const handlePath = (id) => {
        navigate.push(`/blogs/${id}`)
    }

    return (
        <Spin spinning={isLoading}>
            <div className={`${styles.textSection} pd`}>
                <div className={styles.iancard}>
                    <div className={styles.iwr}>
                        <img src={`${MainApi}/${data?.image}`} alt="images"/>
                    </div>
                    <div className={styles.cwr}>
                        <Card/>
                    </div>
                </div>
                <div className={styles.iancard}>
                    <div className={styles.iwr}>
                        <div className={styles.title}
                             dangerouslySetInnerHTML={{__html: `${data?.body?.split('\.', 1)[0]}.`}}/>
                        <div className={styles.body} dangerouslySetInnerHTML={{__html: `${rest}.`}}/>
                    </div>
                    <div className={styles.cwr}>
                        <div className={styles.t12}>
                            RECENT POST
                        </div>
                        <div className={styles.posts}>
                            <ul>
                                {
                                    articles?.data?.data?.sort((a, b) => new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime())?.slice(0, 4)?.map((i, k) => {
                                        return (
                                            <li key={k}>
                                                <p className={styles.lit}
                                                   onClick={() => handlePath(i?.title?.replace(/\s/g, '-'))}>
                                                    {i?.title}
                                                </p>
                                                <p className={styles.lid}>
                                                    {
                                                        `${new Date(i?.createdAt).getUTCFullYear()}-${new Date(i?.createdAt).getMonth() + 1 > 9 ?
                                                            new Date(i?.createdAt).getMonth() + 1 :
                                                            `0${new Date(i?.createdAt).getMonth() + 1}`
                                                        }-${new Date(i?.createdAt).getDate() > 9 ?
                                                            new Date(i?.createdAt).getDate() :
                                                            `0${new Date(i?.createdAt).getDate()}`
                                                        }
                            `
                                                    }
                                                </p>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </Spin>
    );
}

export default TextSection;
