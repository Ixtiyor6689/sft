import React from 'react';
import styles from "./banner.module.scss"
import {BsPersonCircle} from "react-icons/bs"
import {AiOutlineCalendar} from "react-icons/ai"

function Banner({article}) {
    const data = article?.data?.data
    return (
        <div className={styles.bbanner} style={{backgroundImage: `url("/banner.webp")`}}>
            <div className={styles.bg}>
                <div className={styles.con}>
                    <div className={styles.title}>
                        {data?.title}
                    </div>
                    <div className={styles.nrw}>
                        <div className={styles.name}>
                            <BsPersonCircle/> {data?.name}
                        </div>
                        <div className={styles.date}>
                            <AiOutlineCalendar/>
                            {
                            `${new Date(data?.createdAt).getUTCFullYear()}-${(new Date(data?.createdAt).getMonth() + 1) > 9 ?
                                new Date(data?.createdAt).getMonth() + 1:
                                `0${new Date(data?.createdAt).getMonth() + 1}`
                            }-${new Date(data?.createdAt).getDate() > 9 ?
                                new Date(data?.createdAt).getDate():
                                `0${new Date(data?.createdAt).getDate()}`
                            }
                            `
                        }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Banner;
