import React from 'react';
import styles from "./button.module.scss"

function Button({title, onClick, size = "24px",htmlType}) {
    return (
        <button style={{fontSize: size}} className={styles.ubutton} onClick={onClick} type={htmlType}>
            {title}
        </button>
    );
}

export default Button;
