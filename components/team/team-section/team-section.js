import React, {useState} from 'react';
import styles from "./team-section.module.scss"
import Card from "../../services-sections/card/card";
import {data} from "../../teams-sections/data";
import {Empty, Tabs} from 'antd';
import {AiOutlineProfile} from "react-icons/ai";
import {GoGithubAction} from "react-icons/go";
import {MdGroups} from "react-icons/md";
import {IoMdContacts} from "react-icons/io";
import {useRouter} from "next/router";
import TeamReviews from "../../teams-sections/reviews/reviews";


function TeamSection(props) {
    const router = useRouter()
    const one = data?.filter(i => i.name.replace(/\s/g, '').toLowerCase() === router?.query?.slug?.replace(/-/g, '').toLowerCase())[0]
    const [key, setKey] = useState("1")
    const profile = () => {
        return (
            <div className={styles.prteam}>
                Profile
                <div className={styles.line}/>
                <table>
                    {
                        Object.entries(one?.data?.profile ?? {})?.map((k, v) => {
                            return (
                                <tr key={v}>
                                    <td className={styles.label}>{k[0]}</td>
                                    <td className={styles.label}>{k[0] === "Direct" ?
                                        <a href={`tel:${k[1]}`}>{k[1]}</a> : k[1]}</td>
                                </tr>
                            )
                        })
                    }
                </table>
            </div>
        )
    }

    const timeline = () => {
        return (
            <div className={styles.timeline}>
                {
                    one?.data?.timeline?.map((k, v) => {
                        return (
                            <div className={styles.card} key={v}>
                                <div className={styles.imgWr}>
                                    <img
                                        src="/account.png"
                                        alt="s"/>
                                </div>
                                <div className={styles.con}>
                                    <div className={styles.f}>
                                        <span>{one?.name}</span> {k?.action}
                                    </div>
                                    <div className={styles.t}>
                                        {k?.time}
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    const children = [
        profile(),
        timeline(),
        // eslint-disable-next-line react/jsx-key
        <TeamReviews broker={one?.data?.profile?.["First Name"].toLowerCase()}/>,
        // eslint-disable-next-line react/jsx-key
        <Empty/>
    ]

    const list = [
        {
            title: "Profile",
            icon: <AiOutlineProfile/>
        },
        {
            title: "Timeline",
            icon: <GoGithubAction/>
        },
        {
            title: "Reviews",
            icon: <MdGroups/>
        },
        {
            title: "Connections",
            icon: <IoMdContacts/>
        },
    ];

    const handleClick = (e) => {
        router.push(`/team/${router.query?.slug}?defaultKey=${e}`)
        setKey(e)
    }

    return (
        <div className={`${styles.teamSection} pd`}>
            <div className={styles.content}>
                <div className={styles.card}>
                    <Card/>
                </div>
                <div className={styles.info}>
                    <div style={{backgroundImage: `url(/sh1.jpg)`}} className={styles.img}>
                    </div>
                    <div className={styles.bt}>
                        <div className={styles.iwr}>
                            <img
                                src="/account.png"
                                alt="s"/>
                        </div>
                        <div className={styles.name}>
                            <h3 className={styles.law}>
                                {one?.name}
                            </h3>
                            <div className={styles.link}>
                                @{one?.data?.profile?.["First Name"]}
                            </div>
                        </div>
                    </div>
                    <Tabs
                        defaultActiveKey={router?.query?.defaultKey ?? "1"}
                        activeKey={router?.query?.defaultKey ?? key}
                        onTabClick={(e) => handleClick(e)}
                        items={list.map((item, i) => {
                            const id = String(i + 1);
                            return {
                                label: (
                                    <div className={styles.tabMenu}>
                                        {item?.icon} {item?.title}
                                    </div>
                                ),
                                key: id,
                                children: children[i],
                            };
                        })}
                    />
                </div>
            </div>
        </div>
    );
}

export default TeamSection;
