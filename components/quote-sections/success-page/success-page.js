import React from 'react';
import styles from "./success-page.module.scss"

function SuccessPage(props) {
    return (
        <div className={`${styles.successPage} pd`}>
            <div className={styles.imgwr}>
                <img src="/success.jpg" alt="img"/>
            </div>
            <div className={styles.ct}>
                <div className={styles.th}>
                    Thank You
                </div>
                <div className={styles.t1}>
                    WE HAVE SUCCESSFULLY RECEIVED YOUR QUOTE REQUEST.
                </div>
                <div className={styles.tr}>
                    If you need immediate attention, please call us directly at <a href="tel:3153144337">(315)-314-4337</a>.
                </div>
                <div className={styles.tr}>
                    Please check out our FAQ and Supplier Code of Conduct to learn more about auto shipping works.
                </div>
                <div className={styles.tr}>
                    We look forward to assisting you with your transport.
                </div>
            </div>
        </div>
    );
}

export default SuccessPage;
