import React from 'react';
import styles from "./blog.module.scss"
import Banner from "../../../components/blog-section/banner/banner";
import TextSection from "../../../components/blog-section/text-section/text-section";
import ContactUs from "../../../components/home-sections/contacts/contacts";
import Partners from "../../../components/home-sections/partners/partners";
import {useArticle} from "hooks";
import {Spin} from "antd";
import {useRouter} from "next/router";
import Layout from "../../../layout";
import Head from "next/head"
import {MainApi} from "../../../api";
import {dome_url} from "../../../service/api";

const {convert} = require('html-to-text');

function Index(props) {
    const location = useRouter()
    let articles = props?.apiResponse

    const d = articles?.find(i => (
            i.title.replace(/\s/g, '').toLowerCase().toString().at(-1) === "?" ?
                i.title.replace(/\s/g, '').toLowerCase().toString().slice(0, -1) :
                i.title.replace(/\s/g, '').toLowerCase().toString()
        )
        ===
        `${location.query.slug.replace(/-/g, "").toLowerCase().toString()}`)

    const {data: article, isLoading} = useArticle(d?.id)

    let desc = d?.text

    return (
        <Layout>
            <div className={styles.blog}>
                <Head>
                    <meta property="og:type" content="article"/>
                    <meta property="og:title"
                          content={d?.title}
                    />
                    <meta property="og:description"
                          content={desc}
                    />
                    <meta property="og:url" content={`${dome_url}/blogs/${location.query.slug}`}/>
                    <meta name="robots"
                          content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
                    />
                    <meta property="og:site_name" content="Safeeds Transport Inc"/>
                    <meta property="article:publisher"
                          content="https://www.facebook.com/safeedstransportinc"/>
                    <meta property="og:image"
                          content={`${MainApi}/${d?.image}`}
                    />
                    <meta property="og:image:width" content="2000"/>
                    <meta property="og:image:height" content="2000"/>
                    <meta property="og:image:type" content="image/jpg"
                    />
                    <meta name="twitter:site" content="@safeeds"
                    />
                    <meta name="twitter:label1"
                          content="Est. reading time"
                    />
                    <meta name="twitter:data1" content="10 minutes"
                    />
                    <meta
                        name="description"
                        content={desc}

                    />
                    <meta name="twitter:title"
                          content={d?.title}

                    />
                    <meta name="twitter:description"
                          content={desc}

                    />
                    <meta name="twitter:image"
                          content={`${MainApi}/${d?.image}`}

                    />
                    <meta name="twitter:card"
                          content={`${MainApi}/${d?.image}`}

                    />
                    <link rel="apple-touch-icon"
                          href="./logo100.png"/>
                    <link rel="canonical"
                          href={`${dome_url}/blogs/${location.query.slug}`}/>
                    <title>Blogs</title>
                </Head>
                <Spin spinning={isLoading}>
                    <Banner article={article}/>
                </Spin>
                <TextSection article={article}/>
                <ContactUs/>
                <Partners/>
            </div>
        </Layout>
    );
}

export default Index;

export async function getServerSideProps(context) {
    let url = `${MainApi}/article`;

    let requestOptions = {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    };
    const res = await fetch(url, requestOptions);
    const resJson = await res.json();
    let articles = resJson?.data
    let data = []

    for (let i = 0; i < articles?.length; i++) {
        data = [
            ...data,
            {
                ...articles[i],
                text: convert(articles[i]?.body, {
                    wordwrap: 130
                })?.split(
                    /[\.!\?]+/).slice(0, 2)[0] + "." + convert(articles[i]?.body, {
                    wordwrap: 130
                })?.split(
                    /[\.!\?]+/).slice(0, 2)[1]
            }
        ]
    }

    return {
        props: {
            apiResponse: data,
        },
    };
}
