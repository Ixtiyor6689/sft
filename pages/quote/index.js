import React, {useState} from 'react';
import QuoteBanner from "../../components/quote-sections/quote-banner/quote-banner";
import styles from "./quote.module.scss"
import SuccessPage from "../../components/quote-sections/success-page/success-page";
import Layout from "../../layout";
import Head from "next/head";
import {dome_url} from "../../service/api";

function Index(props) {
    const [step, setStep] = useState("quote")

    const Content = () => ({
        quote: <QuoteBanner setStep={setStep}/>,
        success: <SuccessPage/>
    }[step])

    return (
        <Layout>
            <Head>
                <meta
                    name="description"
                    content="One of the leading car transportation companies all over the states. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."
                />
                <meta property="og:type" content="website" />
                <meta property="og:title"
                      content="Get a quote - Safeeds Transport Inc & Auto Shipping broker company"
                />
                <meta property="og:description"
                      content="To get an instant quote for shipping your vehicle just click the link below. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."
                />
                <meta property="og:url" content={`${dome_url}/quote`} />
                <meta name="robots"
                      content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
                />
                <meta property="og:site_name" content="Safeeds Transport Inc" />
                <meta property="article:publisher"
                      content="https://www.facebook.com/safeedstransportinc" />
                <meta property="og:image"
                      content="./img_4.png"
                />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="800" />
                <meta property="og:image:type" content="image/jpg"
                />
                <meta name="twitter:site" content="@safeeds"
                />
                <meta name="twitter:label1"
                      content="Est. reading time"
                />
                <meta name="twitter:data1" content="10 minutes"
                />
                <meta name="twitter:title"
                      content="Get a quote - Safeeds Transport Inc & Auto Shipping broker company | Over 1000+ 5-Star Reviews"

                />
                <meta name="twitter:description"
                      content="To get an instant quote for shipping your vehicle just click the link below. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."

                />
                <meta name="twitter:image"
                      content="./img_4.png"
                />
                <meta name="twitter:card"
                      content="summary_large_image"

                />
                <link rel="apple-touch-icon"
                      href="./logo100.png"/>
                <link rel="canonical"
                      href={`${dome_url}/quote`}/>
                <title>Get a quote - Safeeds Transport</title>
            </Head>
            <div className={styles.quotePg}>
                <Content/>
            </div>
        </Layout>
    );
}

export default React.memo(Index);
