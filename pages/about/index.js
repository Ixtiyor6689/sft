import React from 'react';
import styles from "./about.module.scss"
import ASec1 from "../../components/about-sections/aSec1/aSec1";
import FeatureSection from "../../components/about-sections/feature-sections/feature-section";
import ContactUs from "../../components/home-sections/contacts/contacts";
import Partners from "../../components/home-sections/partners/partners";
import Layout from "../../layout";
import Head from "next/head";
import {dome_url} from "../../service/api";

function Index(props) {
    return (
        <Layout>
            <Head>
                <meta
                    name="description"
                    content="Safeeds Transport Inc has grown to one of the largest auto transport companies in the country in less than five years!"
                />
                <meta property="og:type" content="website" />
                <meta property="og:title"
                      content="About Us - Safeeds Transport Inc & Auto Shipping broker company | Over 1000+ 5-Star Reviews"
                />
                <meta property="og:description"
                      content="Safeeds Transport Inc has grown to one of the largest auto transport companies in the country in less than five years!"
                />
                <meta property="og:url" content={`${dome_url}/about`} />
                <meta name="robots"
                      content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
                />
                <meta property="og:site_name" content="Safeeds Transport Inc" />
                <meta property="article:publisher"
                      content="https://www.facebook.com/safeedstransportinc" />
                <meta property="og:image"
                      content="./covers.jpg"
                />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="800" />
                <meta property="og:image:type" content="image/jpg"
                />
                <meta name="twitter:site" content="@safeeds"
                />
                <meta name="twitter:label1"
                      content="Est. reading time"
                />
                <meta name="twitter:data1" content="10 minutes"
                />
                <meta name="twitter:title"
                      content="About Us - Safeeds Transport Inc & Auto Shipping broker company | Over 1000+ 5-Star Reviews"

                />
                <meta name="twitter:description"
                      content="Safeeds Transport Inc has grown to one of the largest auto transport companies in the country in less than five years!"

                />
                <meta name="twitter:image"
                      content="./covers.jpg"
                />
                <meta name="twitter:card"
                      content="summary_large_image"

                />
                <link rel="apple-touch-icon"
                      href="./logo100.png"/>
                <link rel="canonical"
                      href={`${dome_url}/about`}/>
                <title>About Us - Safeeds Transport Inc</title>
            </Head>
            <div className={styles.about}>
                <ASec1/>
                <FeatureSection/>
                <ContactUs/>
                <Partners/>
            </div>
        </Layout>
    );
}

export default React.memo(Index);
