import React, {useMemo, useState} from 'react';
import {useArticles} from "hooks";
import {Button, Table} from 'antd';
import {MainApi} from "api";
import {PlusCircleOutlined} from "@ant-design/icons";
import Tabs from "../../components/tabs/tabs";
import {useRouter} from "next/router";
import Link from "next/link"
import Admin from "../../components/admin/admin";
import Head from "next/head";

function Index(props) {
    const {data, isLoading} = useArticles()
    const [index, setIndex] = useState(0)
    const navigate = useRouter()

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: (id) => <Link href={`/articles/${id}`}>{id}</Link>,
        },
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Read Time',
            dataIndex: 'readTime',
            key: 'readTime',
        },
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (image) => {
              return(
                  <div>
                      <img src={`${MainApi}/${image}`} alt="image" className="t-img"/>
                  </div>
              )
            },
        },
        {
            title: 'Action',
            key: 'action',
            render: (action) => (
                <Link href={`/articles/${action?.id}`}>
                    <Button type="primary">
                        More
                    </Button>
                </Link>
            ),
        }
    ];

    let dataSource = useMemo(() => {
        return data?.data?.data?.map(item => ({
            key: item?.id,
            id: item.id,
            createdAt: item?.createdAt,
            updatedAt: item?.updatedAt,
            title: item?.title,
            body: item?.body,
            readTime: item?.readTime,
            image: item?.image,
            action: {id: item?.id},
        }))
    }, [data])

    return (
        <Admin>
            <Head>
                <meta name="robots" content="noindex,nofollow" />
                <title>Safeeds Transport</title>
            </Head>
            <Tabs
                index={index}
                setIndex={setIndex}
                tabs={[
                    {
                        title: "Index",
                        content: (
                            <Table columns={columns} dataSource={dataSource} loading={isLoading}/>
                        ),
                    },
                ]}
                tabRightContent={[
                    {
                        icon: <PlusCircleOutlined/>,
                        onClick: () => navigate.push("create-article"),
                    }
                ]}
            />
        </Admin>
    );
}

Index.auth = true
export default Index;
