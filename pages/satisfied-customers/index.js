import React, {useMemo, useState} from 'react';
import {Button, Table} from 'antd';
import {MainApi} from "api";
import {PlusCircleOutlined} from "@ant-design/icons";
import Tabs from "../../components/tabs/tabs";
import {useSatisfieds} from "hooks";
import Admin from "../../components/admin/admin";
import {useRouter} from "next/router";
import Link from "next/link"
import Head from "next/head";

function Index(props) {
    const {data, isLoading} = useSatisfieds()
    const [index, setIndex] = useState(0)
    const navigate = useRouter()

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: (id) => <Link href={`/satisfied-customers/${id}`}>{id}</Link>,
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Star',
            dataIndex: 'star',
            key: 'star',
        },
        {
            title: 'Broker',
            dataIndex: 'broker',
            key: 'broker',
        },
        {
            title: 'Icon',
            dataIndex: 'icon',
            key: 'icon',
            render: (image) => {
              return(
                  <div>
                      <img src={`${MainApi}/${image}`} alt="image" className="t-img"/>
                  </div>
              )
            },
        },
        {
            title: 'Action',
            key: 'action',
            render: (action) => (
                <Link href={`/satisfied-customers/${action?.id}`}>
                    <Button type="primary">
                        More
                    </Button>
                </Link>
            ),
        }
    ];

    let dataSource = useMemo(() => {
        return data?.data?.data?.map(item => ({
            key: item?.id,
            id: item.id,
            createdAt: item?.createdAt,
            updatedAt: item?.updatedAt,
            name: item?.name,
            comment: item?.comment,
            star: item?.star,
            icon: item?.icon,
            broker:item.broker ?? "-",
            action: {id: item?.id},
        }))
    }, [data])

    return (
        <Admin>
            <Head>
                <meta name="robots" content="noindex,nofollow" />
                <title>Safeeds Transport</title>
            </Head>
            <Tabs
                index={index}
                setIndex={setIndex}
                tabs={[
                    {
                        title: "Satisfied Customers",
                        content: (
                            <Table columns={columns} dataSource={dataSource} loading={isLoading}/>
                        ),
                    },
                ]}
                tabRightContent={[
                    {
                        icon: <PlusCircleOutlined/>,
                        onClick: () => navigate.push("create-satisfied-customer"),
                    }
                ]}
            />
        </Admin>
    );
}

Index.auth = true
export default Index;
