import React from 'react';
import Section1 from "../components/home-sections/section-1/section-1";
import styles from "../styles/home.module.scss"
import Reviews from "../components/home-sections/reviews/reviews";
import VideoSection from "../components/home-sections/video-section/video-section";
import Call from "../components/home-sections/call/call";
import Stories from "../components/home-sections/stories/stories";
import Faqs from "../components/home-sections/faqs/faqs";
import ContactUs from "../components/home-sections/contacts/contacts";
import Partners from "../components/home-sections/partners/partners";
import Layout from "../layout";
import Head from "next/head";
import {dome_url} from "../service/api";

function Home(props) {
    return (
        <Layout>
            <Head>
                <meta
                    name="description"
                    content="One of the leading car transportation companies all over the states. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."
                />
                <meta property="og:type" content="website" />
                <meta property="og:title"
                      content="Safeeds Transport Inc & Auto Shipping broker company | Over 1000+ 5-Star Reviews"
                      />
                <meta property="og:description"
                      content="One of the leading car transportation companies all over the states. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."
                      />
                <meta property="og:url" content={dome_url} />
                <meta name="robots"
                      content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
                      />
                <meta property="og:site_name" content="Safeeds Transport Inc" />
                <meta property="article:publisher"
                      content="https://www.facebook.com/safeedstransportinc" />
                <meta property="og:image"
                      content="./covers.jpg"
                      />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="800" />
                <meta property="og:image:type" content="image/jpg"
                      />
                <meta name="twitter:site" content="@safeeds"
                      />
                <meta name="twitter:label1"
                      content="Est. reading time"
                      />
                <meta name="twitter:data1" content="10 minutes"
                      />
                <meta name="twitter:title"
                      content="Safeeds Transport Inc & Auto Shipping broker company | Over 1000+ 5-Star Reviews"

                />
                <meta name="twitter:description"
                      content="One of the leading car transportation companies all over the states. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."

                />
                <meta name="twitter:image"
                      content="./covers.jpg"

                />
                <meta name="twitter:card"
                      content="summary_large_image"

                />
                <link rel="apple-touch-icon"
                      href="./logo100.png"/>
                <link rel="canonical"
                      href={dome_url}/>
                <title>Safeeds Transport</title>
            </Head>
            <div className={styles.home}>
                <Section1/>
                <Reviews/>
                <VideoSection/>
                <Call/>
                <Stories/>
                <Faqs/>
                <ContactUs/>
                <Partners/>
            </div>
        </Layout>
    );
}

export default React.memo(Home);
