import React from 'react';
import S1 from "../../components/services-sections/s1/s1";
import styles from "./services.module.scss"
import S2 from "../../components/services-sections/s2/s2";
import Vd from "../../components/services-sections/vd/vd";
import Cards from "../../components/services-sections/cards/cards";
import SInfo from "../../components/services-sections/sInfo/sInfo";
import ContactSection from "../../components/services-sections/contact-section/contact-section";
import ContactUs from "../../components/home-sections/contacts/contacts";
import Partners from "../../components/home-sections/partners/partners";
import Layout from "../../layout";
import Head from "next/head";
import {dome_url} from "../../service/api";

function Index(props) {
    return (
        <Layout>
            <Head>
                <meta
                    name="description"
                    content="Your vehicle is covered by the carrier’s insurance policy that is doing the shipping. Depending on the type and size of the truck/trailer that is shipping your car, the insurance policy can cover from $100,000 to $1,000,000 of damage."
                />
                <meta property="og:type" content="website" />
                <meta property="og:title"
                      content="Services - Safeeds Transport Inc | Door-To-Door Service"
                />
                <meta property="og:description"
                      content="Your vehicle is covered by the carrier’s insurance policy that is doing the shipping. Depending on the type and size of the truck/trailer that is shipping your car, the insurance policy can cover from $100,000 to $1,000,000 of damage."
                />
                <meta property="og:url" content={`${dome_url}/services`} />
                <meta name="robots"
                      content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
                />
                <meta property="og:site_name" content="Safeeds Transport Inc" />
                <meta property="article:publisher"
                      content="https://www.facebook.com/safeedstransportinc" />
                <meta property="og:image"
                      content="https://safeeds.us/covers.jpg"
                />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="800" />
                <meta property="og:image:type" content="image/jpg"
                />
                <meta name="twitter:site" content="@safeeds"
                />
                <meta name="twitter:label1"
                      content="Est. reading time"
                />
                <meta name="twitter:data1" content="10 minutes"
                />
                <meta name="twitter:title"
                      content="Services - Safeeds Transport Inc | Door-To-Door Service"

                />
                <meta name="twitter:description"
                      content="Your vehicle is covered by the carrier’s insurance policy that is doing the shipping. Depending on the type and size of the truck/trailer that is shipping your car, the insurance policy can cover from $100,000 to $1,000,000 of damage."

                />
                <meta name="twitter:image"
                      content="https://safeeds.us/covers.jpg"
                />
                <meta name="twitter:card"
                      content="summary_large_image"

                />
                <link rel="apple-touch-icon"
                      href="./logo100.png"/>
                <link rel="canonical"
                      href={`${dome_url}/services`}/>
                <title>Services - Safeeds Transport Inc</title>
            </Head>
            <div className={styles.spg}>
                <S1/>
                <S2
                    title="Open Auto Transport"
                    text="Open auto transport carriers are trucks that are used to haul vehicles, as the name says, openly, with no walls around the carrier. They are the trailers you see on the roads every day with cars stacked seemingly precariously on the back of their rigs. They are the most common and easiest-to-find carriers in the transportation industry, moving roughly 90% of all auto freight each year."
                    order={true}
                    picture="./hm.webp"

                />
                <S2
                    title="Enclosed Auto Transport"
                    text="Shipping Enclosed is the safest way to transport a vehicle if you don't want it exposed to the elements. Only the most experienced and highest quality carriers run these exclusive trailers."
                    order={false}
                    picture="./adb.webp"
                />
                <S2
                    title="Flatbed Auto Transport"
                    text="The method you choose – whether open, enclosed, or flatbed – can impact the safety of your vehicle as well as the price and how long it takes to get your car."
                    order={true}
                    picture="./mat.webp"
                />
                <Vd/>
                <Cards/>
                <SInfo/>
                <ContactSection/>
                <ContactUs/>
                <Partners/>
            </div>
        </Layout>
    );
}

export default React.memo(Index);
