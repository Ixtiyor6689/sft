import React from 'react';
import styles from "./contact.module.scss"
import Sw1 from "../../components/contact-sections/sw1/sw1";
import Acmem from "../../components/contact-sections/acmem/acmem";
import ContactSection from "../../components/services-sections/contact-section/contact-section";
import ContactUs from "../../components/home-sections/contacts/contacts";
import Partners from "../../components/home-sections/partners/partners";
import Layout from "../../layout";
import Head from "next/head";
import {dome_url} from "../../service/api";

function Index(props) {
    return (
        <Layout>
            <Head>
                <meta
                    name="description"
                    content="One of the leading car transportation companies all over the states. We ship all types of vehicles CARs SUVs. Motorcycles Trucks ATVs ☎️ (315)314 43 37."
                />
                <meta property="og:type" content="website" />
                <meta property="og:title"
                      content="Contact Us - We're here to help you anytime"
                />
                <meta property="og:description"
                      content="We ship all types of vehicles CARs, SUVs, Motorcycles, Trucks, ATVs. Our team is worldwide and our physical office is located at:1201 Avenue K 3b Brooklyn, NY 11230, United States ."
                />
                <meta property="og:url" content={`${dome_url}/contact`} />
                <meta name="robots"
                      content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
                />
                <meta property="og:site_name" content="Safeeds Transport Inc" />
                <meta property="article:publisher"
                      content="https://www.facebook.com/safeedstransportinc" />
                <meta property="og:image"
                      content="./img_2.png"
                />
                <meta property="og:image:width" content="1200" />
                <meta property="og:image:height" content="800" />
                <meta property="og:image:type" content="image/jpg"
                />
                <meta name="twitter:site" content="@safeeds"
                />
                <meta name="twitter:label1"
                      content="Est. reading time"
                />
                <meta name="twitter:data1" content="10 minutes"
                />
                <meta name="twitter:title"
                      content="Contact Us - We're here to help you anytime"

                />
                <meta name="twitter:description"
                      content="We ship all types of vehicles CARs, SUVs, Motorcycles, Trucks, ATVs. Our team is worldwide and our physical office is located at:1201 Avenue K 3b Brooklyn, NY 11230, United States ."

                />
                <meta name="twitter:image"
                      content="./img_2.png"
                />
                <meta name="twitter:card"
                      content="summary_large_image"

                />
                <link rel="apple-touch-icon"
                      href="./logo100.png"/>
                <link rel="canonical"
                      href={`${dome_url}/contact`}/>
                <title>Contact Us - Safeeds Transport Inc | 315-314-4337</title>
            </Head>
            <div className={styles.contactPage}>
                <Sw1/>
                <Acmem/>
                <ContactSection/>
                <ContactUs/>
                <Partners/>
            </div>
        </Layout>
    );
}

export default React.memo(Index);
