import '../styles/globals.css'
import {store} from "../redux";
import React from "react";
import {Provider} from "react-redux";
import {QueryClient, QueryClientProvider} from 'react-query'
import {ToastContainer} from "react-toastify";
import "assets/var.scss"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import 'antd/dist/antd.css';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import 'react-toastify/dist/ReactToastify.css';
import RequireAuth from "../utils/functions/requireAuth";
import ProgressBar from "@badrap/bar-of-progress";
import Router from "next/router";
import Head from "next/head";

const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            retry: 1,
            refetchOnWindowFocus: false,
        }
    },
})

const progress = new ProgressBar({
    size: 4,
    color: "#2E3192",
    className: "bar-of-progress",
    delay: 100,
});
Router.events.on("routeChangeStart", progress.start);
Router.events.on("routeChangeComplete", progress.finish);
Router.events.on("routeChangeError", progress.finish);

function MyApp({Component, pageProps}) {

    return (
        <>
            <Head>
                <script src="//code.tidio.co/f75we0sspd5pqrakq6xqiu31kvrldjso.js" async></script>
                <meta charSet="utf-8"/>
                <link rel="icon" href="./logo100.png"/>
                <meta name="keywords"
                      content="auto transport company, car shipping company, car shipping companies, companies that ship cars, auto transport companies, car transport companies, best car shipping company, best company to ship a car, car moving companies, car hauling companies, auto shipping companies, vehicle transport company, vehicle transportation company, safeeds, safeeds transport, safeeds transport inc, vehicle shipping, new york auto shipping"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <meta name="theme-color" content="#000000"/>
            </Head>
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    {/*<ScrollToTop />*/}
                    {Component.auth ? (
                        <RequireAuth>
                            <Component {...pageProps} />
                        </RequireAuth>
                    ) : (
                        <Component {...pageProps} />
                    )}
                    <ToastContainer/>
                </QueryClientProvider>
            </Provider>
        </>
    )
}

export default MyApp
