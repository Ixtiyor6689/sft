exports.id = 719;
exports.ids = [719];
exports.modules = {

/***/ 719:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ contact_section)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/services-sections/contact-section/contact-section.module.scss
var contact_section_module = __webpack_require__(8925);
var contact_section_module_default = /*#__PURE__*/__webpack_require__.n(contact_section_module);
;// CONCATENATED MODULE: ./public/disp.jpg
/* harmony default export */ const disp = ({"src":"/_next/static/media/disp.46a69a97.jpg","height":433,"width":650,"blurDataURL":"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoKCgoKCgsMDAsPEA4QDxYUExMUFiIYGhgaGCIzICUgICUgMy03LCksNy1RQDg4QFFeT0pPXnFlZXGPiI+7u/sBCgoKCgoKCwwMCw8QDhAPFhQTExQWIhgaGBoYIjMgJSAgJSAzLTcsKSw3LVFAODhAUV5PSk9ecWVlcY+Ij7u7+//CABEIAAUACAMBIgACEQEDEQH/xAAnAAEBAAAAAAAAAAAAAAAAAAAABQEBAAAAAAAAAAAAAAAAAAAAA//aAAwDAQACEAMQAAAAvg0//8QAHBAAAwABBQAAAAAAAAAAAAAAAQIDBAARITJR/9oACAEBAAE/AMC8jkJVcdEs1CzOAOwG3A8Ov//EABYRAAMAAAAAAAAAAAAAAAAAAAABEf/aAAgBAgEBPwCs/8QAFREBAQAAAAAAAAAAAAAAAAAAAQD/2gAIAQMBAT8AQv/Z","blurWidth":8,"blurHeight":5});
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/services-sections/contact-section/contact-section.js







function ContactSection(props) {
  const navigate = (0,router_.useRouter)();
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: (contact_section_module_default()).contactSection,
    children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
      className: (contact_section_module_default()).bg
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (contact_section_module_default()).content,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (contact_section_module_default()).text,
        children: "Call us 24/7 at 315-314-4337"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (contact_section_module_default()).btiy,
        onClick: () => navigate.push("/quote"),
        style: {
          cursor: "pointer"
        },
        children: "REQUEST A QUOTE"
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: (contact_section_module_default()).img,
      children: /*#__PURE__*/jsx_runtime_.jsx("img", {
        src: "/disp.jpg",
        alt: "rq"
      })
    })]
  });
}

/* harmony default export */ const contact_section = (ContactSection);

/***/ }),

/***/ 8925:
/***/ ((module) => {

// Exports
module.exports = {
	"contactSection": "contact-section_contactSection__YDBNa",
	"bg": "contact-section_bg__l24IQ",
	"content": "contact-section_content__kzzYl",
	"btiy": "contact-section_btiy__Ab3s1",
	"img": "contact-section_img__0w6wI"
};


/***/ })

};
;