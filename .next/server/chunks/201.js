exports.id = 201;
exports.ids = [201];
exports.modules = {

/***/ 8806:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _button_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3413);
/* harmony import */ var _button_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_button_module_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);




function Button({
  title,
  onClick,
  size = "24px",
  htmlType
}) {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("button", {
    style: {
      fontSize: size
    },
    className: (_button_module_scss__WEBPACK_IMPORTED_MODULE_2___default().ubutton),
    onClick: onClick,
    type: htmlType,
    children: title
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Button);

/***/ }),

/***/ 4201:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ card)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./components/services-sections/card/s1/card.module.scss
var card_module = __webpack_require__(7889);
var card_module_default = /*#__PURE__*/__webpack_require__.n(card_module);
// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: ./components/elements/button/button.js
var button_button = __webpack_require__(8806);
// EXTERNAL MODULE: ./components/elements/select/select.js
var select_select = __webpack_require__(5080);
// EXTERNAL MODULE: ./utils/functions/fetchOptions.js
var fetchOptions = __webpack_require__(4713);
// EXTERNAL MODULE: external "react-redux"
var external_react_redux_ = __webpack_require__(6022);
// EXTERNAL MODULE: ./redux/index.js + 3 modules
var redux = __webpack_require__(4439);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/services-sections/card/s1/s1.js
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }













function CardF1({
  setStep
}) {
  const dispatch = (0,external_react_redux_.useDispatch)();
  const navigate = (0,router_.useRouter)();
  const [form] = external_antd_.Form.useForm();
  const {
    0: value,
    1: setValue
  } = (0,external_react_.useState)("Open");
  const {
    0: p,
    1: setP
  } = (0,external_react_.useState)("");
  const {
    0: d,
    1: setD
  } = (0,external_react_.useState)("");

  const onChange = e => {
    setValue(e.target.value);
  };

  const onFinish = values => {
    dispatch((0,redux/* setQuoteLocations */.Ml)(_objectSpread(_objectSpread({}, values), {}, {
      type: value
    })));
    navigate.push("/quote");
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  (0,external_react_.useEffect)(() => {
    if (!!p) {
      form.setFieldValue("pickup", p);
    }

    if (!!d) {
      form.setFieldValue("delivery", d);
    }
  }, [p, d, form]);
  return /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Form, {
    name: "basic",
    labelCol: {
      span: 24
    },
    wrapperCol: {
      span: 24
    },
    initialValues: {
      remember: true
    },
    onFinish: onFinish,
    onFinishFailed: onFinishFailed,
    autoComplete: "off",
    layout: "vertical",
    form: form,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (card_module_default()).mcard,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (card_module_default()).qu,
        children: "Start your free quote"
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (card_module_default()).inputs,
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (card_module_default()).on1,
          children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Form.Item, {
            label: "Pickup location",
            name: "pickup",
            rules: [{
              required: true,
              message: 'Please input your Pickup location!'
            }],
            children: /*#__PURE__*/jsx_runtime_.jsx(select_select/* default */.Z, {
              fetchOptions: search => (0,fetchOptions/* fetchSearchFields */.r)(search, "zipcode"),
              placeholder: "ENTER ZIP CODE OR CITY",
              setP: setP,
              type: "pickup"
            })
          })
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (card_module_default()).on2,
          children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Form.Item, {
            label: "Delivery location",
            name: "delivery",
            rules: [{
              required: true,
              message: 'Please input your Delivery location!'
            }],
            children: /*#__PURE__*/jsx_runtime_.jsx(select_select/* default */.Z, {
              fetchOptions: search => (0,fetchOptions/* fetchSearchFields */.r)(search, "zipcode"),
              placeholder: "ENTER ZIP CODE OR CITY",
              setD: setD,
              type: "delivery"
            })
          })
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (card_module_default()).txt,
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: (card_module_default()).t1,
            children: ["Select ", /*#__PURE__*/jsx_runtime_.jsx("span", {
              children: "Transport Type"
            })]
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (card_module_default()).rd,
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)(external_antd_.Radio.Group, {
              onChange: onChange,
              value: value,
              children: [/*#__PURE__*/jsx_runtime_.jsx(external_antd_.Radio, {
                value: "Open",
                children: "Open"
              }), /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Radio, {
                value: "Enclosed",
                children: "Enclosed"
              })]
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (card_module_default()).on3,
          children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Form.Item, {
            children: /*#__PURE__*/jsx_runtime_.jsx(button_button/* default */.Z, {
              title: "Continue",
              type: "primary",
              htmlType: "submit"
            })
          })
        })]
      })]
    })
  });
}

/* harmony default export */ const s1 = (CardF1);
;// CONCATENATED MODULE: ./components/services-sections/card/card.js




function Card(props) {
  const {
    0: step,
    1: setStep
  } = (0,external_react_.useState)("f1");

  const Content = () => ({
    f1: /*#__PURE__*/jsx_runtime_.jsx(s1, {
      setStep: setStep
    }),
    f2: /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: "some"
    }),
    f3: /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: "some 2"
    })
  })[step];

  return /*#__PURE__*/jsx_runtime_.jsx(Content, {});
}

/* harmony default export */ const card = (Card);

/***/ }),

/***/ 3413:
/***/ ((module) => {

// Exports
module.exports = {
	"ubutton": "button_ubutton__KiaO_"
};


/***/ }),

/***/ 7889:
/***/ ((module) => {

// Exports
module.exports = {
	"mcard": "card_mcard__GZNU1",
	"qu": "card_qu__AkwoI",
	"inputs": "card_inputs___LY8p",
	"on1": "card_on1__5L15_",
	"t1": "card_t1__r1H_4",
	"on2": "card_on2__OUzSJ",
	"on3": "card_on3__r9qgd",
	"txt": "card_txt__HMivB"
};


/***/ })

};
;