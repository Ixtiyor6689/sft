"use strict";
exports.id = 439;
exports.ids = [439];
exports.modules = {

/***/ 4439:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "gN": () => (/* reexport */ _getMe),
  "uJ": () => (/* reexport */ getQuoteLocations),
  "kS": () => (/* reexport */ logout),
  "Ml": () => (/* reexport */ setQuoteLocations),
  "h": () => (/* binding */ store)
});

// UNUSED EXPORTS: _getUnique, getSidebarOpen, login, reset, setToggleSidebar

// EXTERNAL MODULE: external "@reduxjs/toolkit"
var toolkit_ = __webpack_require__(5184);
;// CONCATENATED MODULE: ./redux/slices/reducer/index.js

const initialState = {
  isOpen: false,
  quote: {
    pickup: "",
    delivery: "",
    type: ""
  }
};
const slice = (0,toolkit_.createSlice)({
  name: "main",
  initialState,
  reducers: {
    setToggleSidebar: (store, action) => {
      store.isOpen = action.payload;
    },
    setQuoteLocations: (store, action) => {
      store.quote = action.payload;
    }
  }
});
const {
  setToggleSidebar,
  setQuoteLocations
} = slice.actions;
/* harmony default export */ const reducer = (slice.reducer);
const getSidebarOpen = () => createSelector(store => store.main, state => state.isOpen);
const getQuoteLocations = () => (0,toolkit_.createSelector)(store => store.main, state => state.quote);
// EXTERNAL MODULE: ./utils/cookie/index.js
var cookie = __webpack_require__(5438);
;// CONCATENATED MODULE: ./redux/slices/reducer/auth.js



const loadUser = () => {
  try {
    const serializedUser = (0,cookie/* getCookie */.ej)("user");
    if (!serializedUser) return null;
    return JSON.parse(serializedUser);
  } catch (e) {
    return null;
  }
};

const auth_initialState = {
  user: loadUser(),
  reset_unique: ""
};
/**
 * Actions and Reducers
 */

const authSlice = (0,toolkit_.createSlice)({
  name: "user",
  initialState: auth_initialState,
  reducers: {
    login: (state, action) => {
      (0,cookie/* setCookie */.d8)("user", JSON.stringify(action?.payload));
      state.user = JSON.stringify(action?.payload);
    },
    reset: (state, action) => {
      (0,cookie/* setCookie */.d8)("reset_unique", JSON.stringify(action?.payload?.uniqueId));
      state.reset_unique = JSON.stringify(action?.payload?.uniqueId);
    },
    logout: authStore => {
      (0,cookie/* removeCookie */.nJ)("access_token");
      (0,cookie/* removeCookie */.nJ)("refresh_token");
      (0,cookie/* removeCookie */.nJ)("user");
      authStore.user = null;
    }
  }
});
const {
  login,
  logout,
  reset: auth_reset
} = authSlice.actions;
/* harmony default export */ const auth = (authSlice.reducer); // Selectors

const _getMe = (0,toolkit_.createSelector)(store => store.auth, authStore => authStore.user);
const _getUnique = (0,toolkit_.createSelector)(store => store.auth, authStore => authStore.reset_unique);
;// CONCATENATED MODULE: ./redux/slices/index.js

/** reducers */



/** Main reducer function */

/* harmony default export */ const slices = ((0,toolkit_.combineReducers)({
  main: reducer,
  auth: auth
}));
/** Export selectors and action functions */



;// CONCATENATED MODULE: ./redux/index.js



const store = (0,toolkit_.configureStore)({
  reducer: slices
});

/***/ })

};
;