"use strict";
exports.id = 102;
exports.ids = [102];
exports.modules = {

/***/ 8102:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "At": () => (/* reexport safe */ _useForgotPassword__WEBPACK_IMPORTED_MODULE_4__.At),
/* harmony export */   "FL": () => (/* reexport safe */ _useSatisfied__WEBPACK_IMPORTED_MODULE_2__.FL),
/* harmony export */   "Gx": () => (/* reexport safe */ _useArticles__WEBPACK_IMPORTED_MODULE_1__.Gx),
/* harmony export */   "Oe": () => (/* reexport safe */ _useSatisfied__WEBPACK_IMPORTED_MODULE_2__.Oe),
/* harmony export */   "P0": () => (/* reexport safe */ _useArticles__WEBPACK_IMPORTED_MODULE_1__.P0),
/* harmony export */   "QZ": () => (/* reexport safe */ _useArticles__WEBPACK_IMPORTED_MODULE_1__.QZ),
/* harmony export */   "hC": () => (/* reexport safe */ _useSatisfied__WEBPACK_IMPORTED_MODULE_2__.hC),
/* harmony export */   "kD": () => (/* reexport safe */ _useForgotPassword__WEBPACK_IMPORTED_MODULE_4__.kD),
/* harmony export */   "kR": () => (/* reexport safe */ _useArticles__WEBPACK_IMPORTED_MODULE_1__.kR),
/* harmony export */   "lB": () => (/* reexport safe */ _useEmail__WEBPACK_IMPORTED_MODULE_3__.l),
/* harmony export */   "lN": () => (/* reexport safe */ _useArticles__WEBPACK_IMPORTED_MODULE_1__.lN),
/* harmony export */   "mI": () => (/* reexport safe */ _useForgotPassword__WEBPACK_IMPORTED_MODULE_4__.mI),
/* harmony export */   "rg": () => (/* reexport safe */ _useSatisfied__WEBPACK_IMPORTED_MODULE_2__.rg),
/* harmony export */   "ug": () => (/* reexport safe */ _useArticles__WEBPACK_IMPORTED_MODULE_1__.ug),
/* harmony export */   "yG": () => (/* reexport safe */ _useSatisfied__WEBPACK_IMPORTED_MODULE_2__.yG)
/* harmony export */ });
/* harmony import */ var _useAuth__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(8954);
/* harmony import */ var _useArticles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6846);
/* harmony import */ var _useSatisfied__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(7948);
/* harmony import */ var _useEmail__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(6145);
/* harmony import */ var _useForgotPassword__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(9507);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_useAuth__WEBPACK_IMPORTED_MODULE_0__]);
_useAuth__WEBPACK_IMPORTED_MODULE_0__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];





__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 6846:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Gx": () => (/* binding */ useDeleteArticle),
/* harmony export */   "P0": () => (/* binding */ useUpdateArticle),
/* harmony export */   "QZ": () => (/* binding */ useCreateArticle),
/* harmony export */   "kR": () => (/* binding */ useUploadFile),
/* harmony export */   "lN": () => (/* binding */ useArticles),
/* harmony export */   "ug": () => (/* binding */ useArticle)
/* harmony export */ });
/* unused harmony export getArticle */
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5207);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);




const getArticles = async () => {
  try {
    return await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].get */ .Z.get("/article");
  } catch (err) {
    throw err;
  }
};

const getArticle = async id => {
  try {
    return await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].get */ .Z.get(`/article/${id}`);
  } catch (err) {
    throw err;
  }
};

const updateArticle = async (id, data) => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].put */ .Z.put(`/article/${id}`, data);
  } catch (err) {
    throw err;
  }
};

const deleteArticle = async id => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"]["delete"] */ .Z["delete"](`/article/${id}`);
  } catch (err) {
    throw err;
  }
};

const createArticle = async data => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].post */ .Z.post(`/article`, data);
  } catch (err) {
    throw err;
  }
};

const uploadFile = async data => {
  try {
    return await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].post */ .Z.post(`/image-upload`, data);
  } catch (err) {
    throw err;
  }
};

const useArticles = () => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useQuery)(["rates"], () => getArticles());
};
const useArticle = pid => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const id = pid ?? router?.query.id;
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useQuery)(["rate", id], () => getArticle(id), {
    enabled: !!id
  });
};
const useUpdateArticle = ({
  onSuccess,
  onError
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const id = router?.query.id;
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(data => updateArticle(id, data), {
    onSuccess,
    onError
  });
};
const useDeleteArticle = ({
  onSuccess,
  onError
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const id = router?.query.id;
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(() => deleteArticle(id), {
    onSuccess,
    onError
  });
};
const useCreateArticle = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(data => createArticle(data), {
    onSuccess,
    onError
  });
};
const useUploadFile = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(data => uploadFile(data), {
    onSuccess,
    onError
  });
};

/***/ }),

/***/ 8954:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "z": () => (/* binding */ useSignIn)
/* harmony export */ });
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5207);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3590);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([react_toastify__WEBPACK_IMPORTED_MODULE_2__]);
react_toastify__WEBPACK_IMPORTED_MODULE_2__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];




const signIn = async data => {
  try {
    const res = await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].post */ .Z.post(`/auth/login`, data?.data);
    data?.dispatch({
      type: "user/login",
      payload: res?.data
    });
  } catch (err) {
    react_toastify__WEBPACK_IMPORTED_MODULE_2__.toast.error(err?.data?.error);
    throw err.response;
  }
};

const useSignIn = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(data => signIn(data), {
    onSuccess,
    onError
  });
};
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ }),

/***/ 6145:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "l": () => (/* binding */ useSendEmail)
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5207);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_1__);



const sendEmail = async data => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].post */ .Z.post(`/email/send`, data);
  } catch (err) {
    throw err;
  }
};

const useSendEmail = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_1__.useMutation)(data => sendEmail(data), {
    onSuccess,
    onError
  });
};

/***/ }),

/***/ 9507:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "At": () => (/* binding */ useSendCode),
/* harmony export */   "kD": () => (/* binding */ useCheckCode),
/* harmony export */   "mI": () => (/* binding */ useResetPassword)
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5207);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_1__);



const sendCode = async data => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].post */ .Z.post(`/auth/send-code`, data);
  } catch (err) {
    throw err;
  }
};

const checkCode = async data => {
  try {
    const res = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].post */ .Z.post(`/auth/check-code`, data?.data);
    data?.dispatch({
      type: "user/reset",
      payload: res?.data
    });
  } catch (err) {
    throw err;
  }
};

const resetPassword = async data => {
  try {
    const res = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].put */ .Z.put(`/auth/forget-password`, data?.data);
    data?.dispatch({
      type: "user/login",
      payload: res?.data?.token
    });
  } catch (err) {
    throw err;
  }
};

const useSendCode = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_1__.useMutation)(data => sendCode(data), {
    onSuccess,
    onError
  });
};
const useCheckCode = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_1__.useMutation)(data => checkCode(data), {
    onSuccess,
    onError
  });
};
const useResetPassword = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_1__.useMutation)(data => resetPassword(data), {
    onSuccess,
    onError
  });
};

/***/ }),

/***/ 7948:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FL": () => (/* binding */ useSatisfieds),
/* harmony export */   "Oe": () => (/* binding */ useUpdateSatisfied),
/* harmony export */   "hC": () => (/* binding */ useCreateSatisfied),
/* harmony export */   "rg": () => (/* binding */ useSatisfied),
/* harmony export */   "yG": () => (/* binding */ useDeleteSatisfied)
/* harmony export */ });
/* unused harmony export getSatisfied */
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(1175);
/* harmony import */ var react_query__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_query__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5207);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);




const getSatisfieds = async () => {
  try {
    return await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].get */ .Z.get("/satisfied");
  } catch (err) {
    throw err;
  }
};

const getSatisfied = async id => {
  try {
    return await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].get */ .Z.get(`/satisfied/${id}`);
  } catch (err) {
    throw err;
  }
};

const updateSatisfied = async (id, data) => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].put */ .Z.put(`/satisfied/${id}`, data);
  } catch (err) {
    throw err;
  }
};

const deleteSatisfied = async id => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"]["delete"] */ .Z["delete"](`/satisfied/${id}`);
  } catch (err) {
    throw err;
  }
};

const createSatisfied = async data => {
  try {
    await _api__WEBPACK_IMPORTED_MODULE_1__/* ["default"].post */ .Z.post(`/satisfied`, data);
  } catch (err) {
    throw err;
  }
};

const useSatisfieds = () => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useQuery)(["satisfieds"], () => getSatisfieds());
};
const useSatisfied = pid => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const id = pid ?? router?.query.id;
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useQuery)(["satisfied", id], () => getSatisfied(id), {
    enabled: !!id
  });
};
const useUpdateSatisfied = ({
  onSuccess,
  onError
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const id = router?.query.id;
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(data => updateSatisfied(id, data), {
    onSuccess,
    onError
  });
};
const useDeleteSatisfied = ({
  onSuccess,
  onError
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const id = router?.query.id;
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(() => deleteSatisfied(id), {
    onSuccess,
    onError
  });
};
const useCreateSatisfied = ({
  onSuccess,
  onError
}) => {
  return (0,react_query__WEBPACK_IMPORTED_MODULE_0__.useMutation)(data => createSatisfied(data), {
    onSuccess,
    onError
  });
};

/***/ })

};
;