"use strict";
exports.id = 365;
exports.ids = [365];
exports.modules = {

/***/ 5080:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5725);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(3908);
/* harmony import */ var lodash_debounce__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(lodash_debounce__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__);





function SelectOption({
  fetchOptions,
  afterFetch,
  debounceTimeout = 400,
  placeholder,
  setD,
  setP,
  type,
  defaultValue
}) {
  const {
    0: fetching,
    1: setFetching
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const {
    0: options,
    1: setOptions
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)([]);
  const fetchRef = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)(0);
  const {
    0: value,
    1: setValue
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(undefined);
  const debounceFetcher = (0,react__WEBPACK_IMPORTED_MODULE_0__.useMemo)(() => {
    const loadOptions = (value = "") => {
      fetchRef.current += 1;
      const fetchId = fetchRef.current;
      setOptions([]);
      setFetching(true);
      fetchOptions(value).then(newOptions => {
        if (fetchId !== fetchRef.current) return;
        setOptions(newOptions);
        afterFetch?.(options[0]);
        setFetching(false);
      });
    };

    return lodash_debounce__WEBPACK_IMPORTED_MODULE_2___default()(loadOptions, debounceTimeout);
  }, [debounceTimeout, fetchOptions, afterFetch, options]);

  const onChange = e => {
    setValue(e);

    if (type === "pickup") {
      setP(e);
    }

    if (type === "delivery") {
      setD(e);
    }
  };

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Select, {
    showSearch: true,
    className: "sl",
    style: {
      width: "100%",
      height: "40px"
    },
    placeholder: placeholder,
    allowClear: true,
    filterOption: false,
    onSearch: debounceFetcher,
    onFocus: () => !options.length && debounceFetcher(""),
    notFoundContent: fetching ? /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Spin, {
      size: "small"
    }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Empty, {}),
    options: options,
    onChange: onChange,
    value: value,
    defaultValue: defaultValue
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SelectOption);

/***/ }),

/***/ 4713:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "r": () => (/* binding */ fetchSearchFields)
/* harmony export */ });
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(5207);

/**
 * fetchFields fn
 *
 * @param searchString: Input value.
 * @param urlKey: backend url key.
 */

const fetchSearchFields = async (searchString, urlKey) => {
  try {
    const res = await _api__WEBPACK_IMPORTED_MODULE_0__/* ["default"].post */ .Z.post(`/${urlKey}?search=${searchString}`);
    return res?.data?.city_state?.[0]?.options.map(item => ({
      value: item?._source?.text,
      label: item?._source?.text
    }));
  } catch (err) {
    throw new Error(err);
  }
};

/***/ })

};
;