exports.id = 166;
exports.ids = [166];
exports.modules = {

/***/ 5062:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ layout)
});

// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
// EXTERNAL MODULE: ./layout/header/header.module.scss
var header_module = __webpack_require__(2108);
var header_module_default = /*#__PURE__*/__webpack_require__.n(header_module);
// EXTERNAL MODULE: ./node_modules/next/link.js
var next_link = __webpack_require__(1664);
var link_default = /*#__PURE__*/__webpack_require__.n(next_link);
// EXTERNAL MODULE: external "react-icons/ai"
var ai_ = __webpack_require__(9847);
// EXTERNAL MODULE: external "next/router"
var router_ = __webpack_require__(1853);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./layout/header/header.js








function Header({
  isOpen,
  setIsOpen
}) {
  const navigate = (0,router_.useRouter)();
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: (header_module_default()).header,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `${(header_module_default()).content} pd`,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (header_module_default()).logo,
        children: /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/",
          children: /*#__PURE__*/jsx_runtime_.jsx("img", {
            src: "/logo.png",
            alt: "logo",
            className: (header_module_default()).img
          })
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (header_module_default()).pages,
        children: [/*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/about",
          className: (header_module_default()).lk,
          children: /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (header_module_default()).link,
            children: "About"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/services",
          className: (header_module_default()).lk,
          children: /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (header_module_default()).link,
            children: "Services"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/blogs",
          className: (header_module_default()).lk,
          children: /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (header_module_default()).link,
            children: "Blogs"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/team",
          className: (header_module_default()).lk,
          children: /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (header_module_default()).link,
            children: "Team"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/contact",
          className: (header_module_default()).lk,
          children: /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (header_module_default()).link,
            children: "Contact"
          })
        }), /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/help",
          className: (header_module_default()).lk,
          children: /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (header_module_default()).link,
            children: "Help"
          })
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (header_module_default()).quote,
        onClick: () => navigate.push("/quote"),
        children: /*#__PURE__*/jsx_runtime_.jsx("button", {
          className: (header_module_default()).glowOnHover,
          type: "button",
          children: "GET A QUOTE"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (header_module_default()).menu,
        onClick: () => setIsOpen(!isOpen),
        children: /*#__PURE__*/jsx_runtime_.jsx(ai_.AiOutlineMenu, {
          className: (header_module_default()).icon
        })
      })]
    })
  });
}

/* harmony default export */ const header = (Header);
// EXTERNAL MODULE: external "react-icons/go"
var go_ = __webpack_require__(5856);
// EXTERNAL MODULE: external "react-icons/fa"
var fa_ = __webpack_require__(6290);
// EXTERNAL MODULE: external "react-icons/hi"
var hi_ = __webpack_require__(1111);
// EXTERNAL MODULE: external "react-icons/bs"
var bs_ = __webpack_require__(567);
// EXTERNAL MODULE: ./layout/footer/footer.module.scss
var footer_module = __webpack_require__(1235);
var footer_module_default = /*#__PURE__*/__webpack_require__.n(footer_module);
;// CONCATENATED MODULE: ./layout/footer/footer.js










function Footer(props) {
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: (footer_module_default()).footer,
    children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `${(footer_module_default()).inner} pd`,
      children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (footer_module_default()).col,
        children: [/*#__PURE__*/jsx_runtime_.jsx("img", {
          src: "/yellow-logo.png",
          alt: "footerImg"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (footer_module_default()).txfr,
          children: ["Safeeds Transport Inc is accredited and highly rated with the BBB. We offer REAL Discount car shipping rates and", "Comprehensive Auto Transport Protection with the AFta PLAN. Don't ship your car before you give us a call. We want to earn your Trust. MC # : 01462168 USDOT # : 3938800"]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (footer_module_default()).col,
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (footer_module_default()).title,
          children: "CONTACT US"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (footer_module_default()).list,
          children: [/*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: (footer_module_default()).item,
            children: [/*#__PURE__*/jsx_runtime_.jsx(go_.GoLocation, {
              className: (footer_module_default()).icon
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: "1201 Avenue K 3b, Brooklyn, NY 11230, United States"
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: (footer_module_default()).item,
            children: [/*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillPhone, {
              className: (footer_module_default()).icon
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "tel:3153144337",
                children: "Phone: (315)-314-4337"
              })
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: (footer_module_default()).item,
            children: [/*#__PURE__*/jsx_runtime_.jsx(fa_.FaFax, {
              className: (footer_module_default()).icon
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "tel:3153144337",
                children: "Fax: (315)-314-4337"
              })
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: (footer_module_default()).item,
            children: [/*#__PURE__*/jsx_runtime_.jsx(hi_.HiOutlineMail, {
              className: (footer_module_default()).icon
            }), /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "mailto:info@safeeds.us",
                children: "Email: info@safeeds.us"
              })
            })]
          }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
            className: (footer_module_default()).item,
            children: [/*#__PURE__*/jsx_runtime_.jsx(bs_.BsFillCalendarFill, {
              className: "icon"
            }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: (footer_module_default()).tc,
              children: ["Mon - Fri: 8am - 7pm EST", /*#__PURE__*/jsx_runtime_.jsx("br", {}), "Sat - Sun: 9am - 5pm"]
            })]
          })]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (footer_module_default()).col,
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (footer_module_default()).title,
          children: "REVEWS"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (footer_module_default()).list,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: (footer_module_default()).tc,
              children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
                children: "TRANSPORT REVIEWS"
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: (footer_module_default()).stars,
                children: [/*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                })]
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: (footer_module_default()).nums,
                children: "(225 Reviews)"
              })]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: (footer_module_default()).tc,
              children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
                children: "FACEBOOK"
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: (footer_module_default()).stars,
                children: [/*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                })]
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: (footer_module_default()).nums,
                children: "(170 Reviews)"
              })]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: (footer_module_default()).tc,
              children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
                children: "GOOGLE"
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: (footer_module_default()).stars,
                children: [/*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                })]
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: (footer_module_default()).nums,
                children: "(284 Reviews)"
              })]
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
              className: (footer_module_default()).tc,
              children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
                children: "BBB"
              }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
                className: (footer_module_default()).stars,
                children: [/*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                }), /*#__PURE__*/jsx_runtime_.jsx(ai_.AiFillStar, {
                  className: (footer_module_default()).star
                })]
              }), /*#__PURE__*/jsx_runtime_.jsx("div", {
                className: (footer_module_default()).nums,
                children: "(136 Reviews)"
              })]
            })
          })]
        })]
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (footer_module_default()).col,
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (footer_module_default()).title,
          children: "QUICK LINKS"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (footer_module_default()).list,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC",
                children: "Our Licence"
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "/terms-and-conditions",
                children: "Terms & Conditions"
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                children: "Condition Report & Checklist"
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "",
                children: "Car Shipping Analytics"
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "",
                children: "How Auto Transport Works"
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "",
                children: "How We Determine Your Quote"
              })
            })
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (footer_module_default()).item,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (footer_module_default()).tc,
              children: /*#__PURE__*/jsx_runtime_.jsx("a", {
                href: "",
                children: "The Safeeds Guarantee"
              })
            })
          })]
        })]
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `${(footer_module_default()).line} pd`
    }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: `${(footer_module_default()).term} pd`,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "Website Terms"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "Privacy Policy"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "Accessibility Statement"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "CA Transparency in Supply Chains Act"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "Supplier Code of Conduct"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "Marketing to Children"
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (footer_module_default()).chl,
        children: "Do Not Sell My Information"
      })]
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: `${(footer_module_default()).rg} pd`,
      children: "\xA92022 Safeeds Transport Inc, LLC. All Rights Reserved"
    })]
  });
}

/* harmony default export */ const footer = (Footer);
// EXTERNAL MODULE: ./layout/sidebar/sidebar.module.scss
var sidebar_module = __webpack_require__(9528);
var sidebar_module_default = /*#__PURE__*/__webpack_require__.n(sidebar_module);
;// CONCATENATED MODULE: ./layout/sidebar/sidebar.js







function Sidebar({
  isOpen,
  setIsOpen
}) {
  const navigate = (0,router_.useRouter)();

  const handleNavigate = path => {
    setIsOpen(false);
    navigate.push(path);
  };

  const handlePath = () => {
    navigate.push("/quote");
    setIsOpen(false);
  };

  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: `${(sidebar_module_default()).sidebar} ${isOpen ? (sidebar_module_default()).active : ""}`,
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
      className: (sidebar_module_default()).sio,
      children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (sidebar_module_default()).lg,
        children: /*#__PURE__*/jsx_runtime_.jsx((link_default()), {
          href: "/",
          onClick: () => handleNavigate("/"),
          children: /*#__PURE__*/jsx_runtime_.jsx("img", {
            src: "/logo.png",
            alt: "logo"
          })
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
        children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/about"),
          children: "About"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/services"),
          children: "Services"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/blogs"),
          children: "Blogs"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/team"),
          children: "Team"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/contact"),
          children: "Contact"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/help"),
          children: "Help"
        })]
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (sidebar_module_default()).bt,
        children: /*#__PURE__*/jsx_runtime_.jsx("button", {
          className: (sidebar_module_default()).glowOnHover,
          type: "button",
          onClick: () => handlePath(),
          children: "GET A QUOTE"
        })
      }), /*#__PURE__*/jsx_runtime_.jsx("div", {
        className: (sidebar_module_default()).tel,
        children: /*#__PURE__*/jsx_runtime_.jsx("a", {
          href: "tel:315-314-4337",
          children: "315-314-4337"
        })
      }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("ul", {
        className: (sidebar_module_default()).ntr,
        children: [/*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC"),
          children: "Our license"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/"),
          children: "Terms & Conditions"
        }), /*#__PURE__*/jsx_runtime_.jsx("li", {
          onClick: () => handleNavigate("/"),
          children: "Contact us"
        })]
      })]
    })
  });
}

/* harmony default export */ const sidebar = (Sidebar);
;// CONCATENATED MODULE: ./layout/index.js







function Layout(props) {
  const {
    0: isOpen,
    1: setIsOpen
  } = (0,external_react_.useState)(false);
  const styles = {
    position: "fixed",
    width: "100%",
    height: "100vh",
    top: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,0.4)",
    zIndex: 1200,
    display: isOpen ? "block" : "none"
  };
  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: "layout",
    children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
      style: styles,
      onClick: () => setIsOpen(!isOpen)
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: /*#__PURE__*/jsx_runtime_.jsx(sidebar, {
        isOpen: isOpen,
        setIsOpen: setIsOpen
      })
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "head",
      children: /*#__PURE__*/jsx_runtime_.jsx(header, {
        isOpen: isOpen,
        setIsOpen: setIsOpen
      })
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: "w100",
      children: props.children
    }), /*#__PURE__*/jsx_runtime_.jsx("div", {
      children: /*#__PURE__*/jsx_runtime_.jsx(footer, {})
    })]
  });
}

/* harmony default export */ const layout = (Layout);

/***/ }),

/***/ 6210:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "_": () => (/* binding */ dome_url)
/* harmony export */ });
const dome_url = "https://safeeds.us/";

/***/ }),

/***/ 1235:
/***/ ((module) => {

// Exports
module.exports = {
	"footer": "footer_footer__QO2rN",
	"inner": "footer_inner__Zu405",
	"col": "footer_col__XMyhI",
	"txfr": "footer_txfr__rmLBx",
	"title": "footer_title__Bj28c",
	"item": "footer_item__Q3G1S",
	"icon": "footer_icon__3DFfE",
	"tc": "footer_tc__s_CHB",
	"stars": "footer_stars__7qpmC",
	"star": "footer_star__bUAYR",
	"line": "footer_line__IOCHZ",
	"term": "footer_term__APyLg",
	"chl": "footer_chl__OD1lH",
	"rg": "footer_rg__kaeGf"
};


/***/ }),

/***/ 2108:
/***/ ((module) => {

// Exports
module.exports = {
	"header": "header_header__Be4gv",
	"content": "header_content__TUEdA",
	"logo": "header_logo__nOf99",
	"img": "header_img__pNK5J",
	"pages": "header_pages__ssyQ_",
	"lk": "header_lk__fdbyO",
	"link": "header_link__LFiU5",
	"quote": "header_quote__aCkzm",
	"glowOnHover": "header_glowOnHover__iA6e_",
	"glowing": "header_glowing__l9CyN",
	"menu": "header_menu___DtMf",
	"icon": "header_icon__5HwvL"
};


/***/ }),

/***/ 9528:
/***/ ((module) => {

// Exports
module.exports = {
	"sidebar": "sidebar_sidebar__ZhF68",
	"active": "sidebar_active__pDhUo",
	"sio": "sidebar_sio__sIa1J",
	"lg": "sidebar_lg__sx7sn",
	"ntr": "sidebar_ntr__qSgcy",
	"bt": "sidebar_bt__tnkiq",
	"glowOnHover": "sidebar_glowOnHover__gK0i7",
	"glowing": "sidebar_glowing__M6w2u",
	"tel": "sidebar_tel__Gd1X1"
};


/***/ })

};
;