"use strict";
exports.id = 614;
exports.ids = [614];
exports.modules = {

/***/ 1614:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {


// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ tabs)
});

// EXTERNAL MODULE: external "antd"
var external_antd_ = __webpack_require__(5725);
// EXTERNAL MODULE: external "styled-components"
var external_styled_components_ = __webpack_require__(7518);
var external_styled_components_default = /*#__PURE__*/__webpack_require__.n(external_styled_components_);
;// CONCATENATED MODULE: ./components/tabs/tabs.e.js

const Wrapper = external_styled_components_default().div.withConfig({
  displayName: "tabse__Wrapper",
  componentId: "sc-1e9to1r-0"
})([""]);
const Icon = external_styled_components_default().div.withConfig({
  displayName: "tabse__Icon",
  componentId: "sc-1e9to1r-1"
})(["margin:0 5px;cursor:pointer;transition:all 0.4s ease;display:inline-block;& > span{font-size:20px;}&:hover{opacity:0.6;transform:scale(1.05);}"]);
const RelativeParent = external_styled_components_default().span.withConfig({
  displayName: "tabse__RelativeParent",
  componentId: "sc-1e9to1r-2"
})(["position:relative;"]);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/tabs/tabs.js




const Tabs = ({
  tabs,
  index,
  tabRightContent,
  setIndex
}) => {
  const OperationsSlot = {
    right: tabRightContent?.map((rightTab, index) => /*#__PURE__*/jsx_runtime_.jsx(RelativeParent, {
      children: /*#__PURE__*/jsx_runtime_.jsx(Icon, {
        onClick: () => rightTab?.onClick?.(),
        children: rightTab.icon
      }, index)
    }, index))
  };
  return /*#__PURE__*/jsx_runtime_.jsx(Wrapper, {
    children: /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Tabs, {
      animated: true,
      activeKey: index.toString(),
      onChange: key => setIndex(parseInt(key)),
      tabBarExtraContent: OperationsSlot,
      children: tabs.map((tab, index) => /*#__PURE__*/jsx_runtime_.jsx(external_antd_.Tabs.TabPane, {
        tab: tab.title,
        children: tab.content
      }, index))
    })
  });
};

/* harmony default export */ const tabs = (Tabs);

/***/ })

};
;