exports.id = 714;
exports.ids = [714];
exports.modules = {

/***/ 7674:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";

// EXPORTS
__webpack_require__.d(__webpack_exports__, {
  "Z": () => (/* binding */ ContactUs)
});

// EXTERNAL MODULE: ./components/home-sections/contacts/contacts.module.scss
var contacts_module = __webpack_require__(3846);
var contacts_module_default = /*#__PURE__*/__webpack_require__.n(contacts_module);
// EXTERNAL MODULE: external "react"
var external_react_ = __webpack_require__(6689);
var external_react_default = /*#__PURE__*/__webpack_require__.n(external_react_);
// EXTERNAL MODULE: external "@react-google-maps/api"
var api_ = __webpack_require__(2433);
// EXTERNAL MODULE: external "react/jsx-runtime"
var jsx_runtime_ = __webpack_require__(997);
;// CONCATENATED MODULE: ./components/home-sections/contacts/map.js




const containerStyle = {
  width: '100%',
  height: '80vh'
};
const center = {
  lat: 40.62284628985301,
  lng: -73.96335934418308
};
const markers = [{
  id: 1,
  name: "Nyu York",
  position: {
    lat: 40.62284628985301,
    lng: -73.96335934418308
  }
}];

function MyComponent() {
  const {
    isLoaded
  } = (0,api_.useJsApiLoader)({
    id: 'google-map-script',
    googleMapsApiKey: "AIzaSyCB1vQhgArG3pHSL4T8_UB_E5Vghji8BBE"
  });

  const handleOnLoad = map => {
    map.setZoom(11);
  };

  return isLoaded ? /*#__PURE__*/jsx_runtime_.jsx(api_.GoogleMap, {
    mapContainerStyle: containerStyle,
    center: center,
    options: {
      zoom: 11
    },
    onLoad: handleOnLoad,
    children: /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {
      children: markers.map(({
        id,
        position
      }) => /*#__PURE__*/jsx_runtime_.jsx(api_.MarkerF, {
        position: position
      }, id))
    })
  }) : /*#__PURE__*/jsx_runtime_.jsx(jsx_runtime_.Fragment, {});
}

/* harmony default export */ const map = (/*#__PURE__*/external_react_default().memo(MyComponent));
;// CONCATENATED MODULE: ./assets/vectorIcons/location.jsx




const Location = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("svg", {
      id: "location",
      xmlns: "http://www.w3.org/2000/svg",
      width: "20",
      height: "20",
      viewBox: "0 0 20 20",
      children: [/*#__PURE__*/jsx_runtime_.jsx("rect", {
        id: "Rectangle_4497",
        "data-name": "Rectangle 4497",
        width: "20",
        height: "20",
        fill: "#263238",
        opacity: "0"
      }), /*#__PURE__*/jsx_runtime_.jsx("path", {
        id: "ic_place_24px",
        d: "M11,2A6.151,6.151,0,0,0,5,8.3C5,13.025,11,20,11,20s6-6.975,6-11.7A6.151,6.151,0,0,0,11,2Zm0,8.55A2.2,2.2,0,0,1,8.857,8.3,2.2,2.2,0,0,1,11,6.05,2.2,2.2,0,0,1,13.143,8.3,2.2,2.2,0,0,1,11,10.55Z",
        transform: "translate(-1 -1)",
        fill: "#263238"
      })]
    })
  });
};

/* harmony default export */ const vectorIcons_location = (Location);
;// CONCATENATED MODULE: ./assets/vectorIcons/emailIcon.jsx



const EmailIcon = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("svg", {
      id: "emailIcon",
      xmlns: "http://www.w3.org/2000/svg",
      width: "20",
      height: "20",
      viewBox: "0 0 20 20",
      children: [/*#__PURE__*/jsx_runtime_.jsx("rect", {
        id: "Rectangle_4499",
        "data-name": "Rectangle 4499",
        width: "20",
        height: "20",
        fill: "#fff",
        opacity: "0"
      }), /*#__PURE__*/jsx_runtime_.jsx("path", {
        id: "Icon",
        d: "M844.882,427.526h-19v-.01l5.794-5.692,1.189,1.2a3.544,3.544,0,0,0,5.011.024l.024-.024,1.19-1.2,5.794,5.692Zm-19.491-.589h-.01V415.385h.058c.471.5,2.406,2.465,5.751,5.846Zm19.99-.05h-.01l-5.8-5.7c3.308-3.347,5.3-5.369,5.752-5.846h.058v11.546Zm-10-3.745a2.827,2.827,0,0,1-2.021-.846l-7.408-7.455c.542-.007,2.238-.007,5.044-.007,2.258,0,6.452,0,13.615.008h.193l-7.4,7.453A2.829,2.829,0,0,1,835.381,423.142Z",
        transform: "translate(-825.381 -410.834)",
        fill: "#263238"
      })]
    })
  });
};

/* harmony default export */ const emailIcon = (EmailIcon);
;// CONCATENATED MODULE: ./assets/vectorIcons/phoneIcon.jsx




const PhoneIcon = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("svg", {
      id: "phoneIcon",
      xmlns: "http://www.w3.org/2000/svg",
      width: "20",
      height: "20",
      viewBox: "0 0 20 20",
      children: [/*#__PURE__*/jsx_runtime_.jsx("rect", {
        id: "Rectangle_4497",
        "data-name": "Rectangle 4497",
        width: "20",
        height: "20",
        fill: "#263238",
        opacity: "0"
      }), /*#__PURE__*/jsx_runtime_.jsx("path", {
        id: "Icon",
        d: "M1.176,5.924A8.022,8.022,0,0,1,0,2.294c0-.841.207-1.082.416-1.277S1.571.24,1.732.131A.858.858,0,0,1,2.893.366C3.278.92,4,2.034,4.5,2.773c.785,1.047.16,1.507-.042,1.783-.372.507-.586.632-.586,1.255S5.617,8.2,6.033,8.639a10.708,10.708,0,0,0,2.684,2.034,2.242,2.242,0,0,0,1.42-.636c.784-.6,1.226-.145,1.587.053s1.992,1.214,2.5,1.557a1.072,1.072,0,0,1,.446.872s-.981,1.557-1.1,1.744c-.149.218-.505.4-1.308.4s-1.66-.147-3.7-1.267a19.653,19.653,0,0,1-4.089-3.194A20.323,20.323,0,0,1,1.176,5.924Z",
        transform: "translate(2.667 2.667)",
        fill: "#263238"
      })]
    })
  });
};

/* harmony default export */ const phoneIcon = (PhoneIcon);
;// CONCATENATED MODULE: ./assets/vectorIcons/facebookteamdefult1.jsx




const Facebookteamdefult1 = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "bgi",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("svg", {
      id: "facebookteamdefult",
      xmlns: "http://www.w3.org/2000/svg",
      width: "24",
      height: "24",
      viewBox: "0 0 24 24",
      children: [/*#__PURE__*/jsx_runtime_.jsx("rect", {
        id: "Rectangle_4569",
        "data-name": "Rectangle 4569",
        width: "24",
        height: "24",
        opacity: "0"
      }), /*#__PURE__*/jsx_runtime_.jsx("path", {
        id: "Facebook",
        d: "M116.31,3.1V5.3H114.7V8h1.614v8h3.315V8h2.225s.208-1.292.309-2.7h-2.522V3.451a.782.782,0,0,1,.719-.646h1.806V0h-2.456C116.228,0,116.31,2.7,116.31,3.1Z",
        transform: "translate(-106.696 3.999)",
        fill: "#d3d6d7"
      })]
    })
  });
};

/* harmony default export */ const facebookteamdefult1 = (Facebookteamdefult1);
;// CONCATENATED MODULE: ./assets/vectorIcons/instagramteamdefult1.jsx




const Instagramteamdefult1 = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "bgi",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("svg", {
      id: "instagramteamdefult",
      xmlns: "http://www.w3.org/2000/svg",
      width: "24",
      height: "24",
      viewBox: "0 0 24 24",
      fill: "red",
      children: [/*#__PURE__*/jsx_runtime_.jsx("rect", {
        id: "Rectangle_4571",
        "data-name": "Rectangle 4571",
        width: "24",
        height: "24",
        opacity: "0"
      }), /*#__PURE__*/jsx_runtime_.jsx("path", {
        id: "instagram",
        d: "M4.415,16A4.421,4.421,0,0,1,0,11.584V4.415A4.421,4.421,0,0,1,4.415,0h7.169A4.42,4.42,0,0,1,16,4.415v7.169A4.42,4.42,0,0,1,11.585,16Zm-3-11.585v7.169a3,3,0,0,0,3,3h7.169a3,3,0,0,0,3-3V4.415a3,3,0,0,0-3-3H4.415A3,3,0,0,0,1.42,4.415ZM3.877,8A4.123,4.123,0,1,1,8,12.123,4.128,4.128,0,0,1,3.877,8ZM5.3,8A2.7,2.7,0,1,0,8,5.3,2.707,2.707,0,0,0,5.3,8ZM11.56,4.451a1.04,1.04,0,1,1,.735.3A1.048,1.048,0,0,1,11.56,4.451Z",
        transform: "translate(4 4)",
        fill: "#d3d6d7"
      })]
    })
  });
};

/* harmony default export */ const instagramteamdefult1 = (Instagramteamdefult1);
;// CONCATENATED MODULE: ./assets/vectorIcons/twittertamdefult1.jsx




const Twittertamdefult1 = () => {
  return /*#__PURE__*/jsx_runtime_.jsx("div", {
    className: "bgi",
    children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("svg", {
      id: "twittertamdefult",
      xmlns: "http://www.w3.org/2000/svg",
      width: "24",
      height: "24",
      viewBox: "0 0 24 24",
      children: [/*#__PURE__*/jsx_runtime_.jsx("rect", {
        id: "Rectangle_4570",
        "data-name": "Rectangle 4570",
        width: "24",
        height: "24",
        opacity: "0"
      }), /*#__PURE__*/jsx_runtime_.jsx("path", {
        id: "twitter",
        d: "M16,58.979a6.555,6.555,0,0,1-1.885.517,3.3,3.3,0,0,0,1.443-1.815,6.617,6.617,0,0,1-2.086.8A3.285,3.285,0,0,0,7.88,61.47a9.318,9.318,0,0,1-6.765-3.429,3.286,3.286,0,0,0,1.016,4.381A3.283,3.283,0,0,1,.645,62.01v.041A3.286,3.286,0,0,0,3.277,65.27a3.324,3.324,0,0,1-.865.115,3.151,3.151,0,0,1-.618-.061A3.284,3.284,0,0,0,4.86,67.6a6.585,6.585,0,0,1-4.076,1.4A6.974,6.974,0,0,1,0,68.961a9.278,9.278,0,0,0,5.031,1.477A9.274,9.274,0,0,0,14.37,61.1l-.011-.425A6.554,6.554,0,0,0,16,58.979Z",
        transform: "translate(3.999 -52.441)",
        fill: "#d3d6d7"
      })]
    })
  });
};

/* harmony default export */ const twittertamdefult1 = (Twittertamdefult1);
;// CONCATENATED MODULE: ./components/home-sections/contacts/contacts.js










function ContactUs() {
  const instaGram = () => {
    // window.location.href = "https://youtu.be/CRtB7DZJqHk";
    window.open("https://www.instagram.com/safeedstransportion");
  };

  const faceBook = () => {
    // window.location.href = "https://youtu.be/CRtB7DZJqHk";
    window.open("https://www.facebook.com/safeedstransportinc");
  };

  const twitTer = () => {
    // window.location.href = "https://youtu.be/CRtB7DZJqHk";
    window.open("https://twitter.com/safeedsus");
  };

  const sendMail = () => {
    window.location.href = "mailto:contact@safeeds.us?subject=AutoShipping";
  };

  return /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
    className: (contacts_module_default()).wrapper,
    id: "section-7",
    children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
      className: (contacts_module_default()).tit,
      children: "Our contacts:"
    }), /*#__PURE__*/jsx_runtime_.jsx(map, {}), /*#__PURE__*/jsx_runtime_.jsx("div", {
      className: (contacts_module_default()).left,
      children: /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
        className: (contacts_module_default()).inner,
        children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (contacts_module_default()).title,
          children: "Contact us"
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (contacts_module_default()).htitle,
          children: "Headquarters"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (contacts_module_default()).adress,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).contactIcon,
            children: /*#__PURE__*/jsx_runtime_.jsx(vectorIcons_location, {})
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).text,
            children: "1201 Avenue K 3b, Brooklyn, NY 11230, United States"
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (contacts_module_default()).adress,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).contactIcon,
            children: /*#__PURE__*/jsx_runtime_.jsx(emailIcon, {})
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).text,
            children: /*#__PURE__*/jsx_runtime_.jsx("div", {
              className: (contacts_module_default()).email,
              onClick: sendMail,
              children: "contact@safeeds.us"
            })
          })]
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (contacts_module_default()).adress,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).contactIcon,
            children: /*#__PURE__*/jsx_runtime_.jsx(phoneIcon, {})
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).text,
            children: /*#__PURE__*/jsx_runtime_.jsx("a", {
              className: (contacts_module_default()).phoneNumber,
              href: "tel:3153144337",
              children: "(315)-314-4337"
            })
          })]
        }), /*#__PURE__*/jsx_runtime_.jsx("div", {
          className: (contacts_module_default()).htitle,
          children: "Social networks"
        }), /*#__PURE__*/(0,jsx_runtime_.jsxs)("div", {
          className: (contacts_module_default()).icon,
          children: [/*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).iconIn,
            onClick: faceBook,
            children: /*#__PURE__*/jsx_runtime_.jsx(facebookteamdefult1, {})
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).iconIn,
            onClick: instaGram,
            children: /*#__PURE__*/jsx_runtime_.jsx(instagramteamdefult1, {})
          }), /*#__PURE__*/jsx_runtime_.jsx("div", {
            className: (contacts_module_default()).iconIn,
            onClick: twitTer,
            children: /*#__PURE__*/jsx_runtime_.jsx(twittertamdefult1, {})
          })]
        })]
      })
    })]
  });
}

/***/ }),

/***/ 512:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _partners_module_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(298);
/* harmony import */ var _partners_module_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_partners_module_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(8096);
/* harmony import */ var react_slick__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_slick__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }







function Partners(props) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
        infinite: true,
        dots: true
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  };
  const data = [{
    img: "/partnerImgs/slide2.png",
    link: "https://www.mymovingreviews.com/car-movers/safeeds-transport-inc-33673"
  }, {
    img: "/partnerImgs/slide3.png",
    link: "https://www.copart.com"
  }, {
    img: "/partnerImgs/slide4.png",
    link: "https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC"
  }, {
    img: "/partnerImgs/slide5.png",
    link: "https://www.transportreviews.com/Company/Safeeds-Transport-Inc"
  }, {
    img: "/partnerImgs/slide7.png",
    link: "https://safer.fmcsa.dot.gov/query.asp?searchtype=ANY&query_type=queryCarrierSnapshot&query_param=USDOT&original_query_param=NAME&query_string=3938800&original_query_string=SAFEEDS%20TRANSPORT%20INC"
  }, {
    img: "/partnerImgs/bbb.png",
    link: "https://www.bbb.org/us/ny/brooklyn/profile/trucking-transportation-brokers/safeeds-transport-inc-0121-87157757"
  }];

  const handleNavigate = link => {
    window.location.href = link;
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
    className: `${(_partners_module_scss__WEBPACK_IMPORTED_MODULE_3___default().partners)} pd`,
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("div", {
      className: (_partners_module_scss__WEBPACK_IMPORTED_MODULE_3___default().partner),
      children: "OUR PARTNERS:"
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("div", {
      className: "slider",
      children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx((react_slick__WEBPACK_IMPORTED_MODULE_1___default()), _objectSpread(_objectSpread({}, settings), {}, {
        children: data?.map((i, k) => {
          return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("div", {
            className: "mlr20",
            onClick: () => handleNavigate(i?.link),
            children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx("img", {
              src: i.img,
              alt: `img${k}`,
              className: "bgfd"
            })
          }, k);
        })
      }))
    })]
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Partners);

/***/ }),

/***/ 3846:
/***/ ((module) => {

// Exports
module.exports = {
	"wrapper": "contacts_wrapper__uyQV1",
	"tit": "contacts_tit__0y7pc",
	"contact": "contacts_contact__noSsx",
	"footer": "contacts_footer__k5Kja",
	"left": "contacts_left__eE7Ay",
	"inner": "contacts_inner__z0OxV",
	"title": "contacts_title__ZsTiL",
	"htitle": "contacts_htitle__O4799",
	"icon": "contacts_icon__X9u5d",
	"iconIn": "contacts_iconIn__DOf_b",
	"adress": "contacts_adress__ZpSNK",
	"text": "contacts_text__glIwr",
	"email": "contacts_email__RIAGu",
	"phoneNumber": "contacts_phoneNumber__OdDV7"
};


/***/ }),

/***/ 298:
/***/ ((module) => {

// Exports
module.exports = {
	"partners": "partners_partners__GG6SD",
	"partner": "partners_partner__JRv13"
};


/***/ })

};
;