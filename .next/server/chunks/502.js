"use strict";
exports.id = 502;
exports.ids = [502];
exports.modules = {

/***/ 6542:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "C": () => (/* binding */ InputPassword)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_input_Password__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(4157);
/* harmony import */ var antd_lib_input_Password__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_input_Password__WEBPACK_IMPORTED_MODULE_1__);


const InputPassword = styled_components__WEBPACK_IMPORTED_MODULE_0___default()((antd_lib_input_Password__WEBPACK_IMPORTED_MODULE_1___default())).withConfig({
  displayName: "authe__InputPassword",
  componentId: "sc-147u666-0"
})([".ant-input{height:30px !important;}"]);

/***/ }),

/***/ 6502:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(5725);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _hooks__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(8102);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(3590);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _utils_cookie__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(5438);
/* harmony import */ var _components_styled_auth_e__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(6542);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(1853);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_hooks__WEBPACK_IMPORTED_MODULE_2__, react_toastify__WEBPACK_IMPORTED_MODULE_3__]);
([_hooks__WEBPACK_IMPORTED_MODULE_2__, react_toastify__WEBPACK_IMPORTED_MODULE_3__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);











function PutPassword(props) {
  const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_4__.useDispatch)();
  const navigate = (0,next_router__WEBPACK_IMPORTED_MODULE_6__.useRouter)();
  const resetPassword = (0,_hooks__WEBPACK_IMPORTED_MODULE_2__/* .useResetPassword */ .mI)({
    onSuccess() {
      react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast.success("Successfully reset");
      navigate.push("/articles");
      (0,_utils_cookie__WEBPACK_IMPORTED_MODULE_8__/* .removeCookie */ .nJ)("reset_unique");
    },

    onError(err) {
      react_toastify__WEBPACK_IMPORTED_MODULE_3__.toast.error(err?.data?.error);
    }

  });

  const onFinish = values => {
    resetPassword.mutate({
      data: {
        password: values?.password,
        uniqueId: (0,_utils_cookie__WEBPACK_IMPORTED_MODULE_8__/* .getCookie */ .ej)("reset_unique").substring(1, (0,_utils_cookie__WEBPACK_IMPORTED_MODULE_8__/* .getCookie */ .ej)("reset_unique")?.length - 1)
      },
      dispatch: dispatch
    });
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx("div", {
    className: "cel",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)(antd__WEBPACK_IMPORTED_MODULE_1__.Form, {
      name: "basic",
      initialValues: {
        remember: true
      },
      onFinish: onFinish,
      onFinishFailed: onFinishFailed,
      autoComplete: "off",
      layout: "vertical",
      className: "form",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Form.Item, {
        name: "password",
        label: "Password",
        rules: [{
          required: true,
          message: 'Please input your password!'
        }],
        hasFeedback: true,
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(_components_styled_auth_e__WEBPACK_IMPORTED_MODULE_5__/* .InputPassword */ .C, {})
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Form.Item, {
        name: "confirm",
        label: "Confirm Password",
        dependencies: ['password'],
        hasFeedback: true,
        rules: [{
          required: true,
          message: 'Please confirm your password!'
        }, ({
          getFieldValue
        }) => ({
          validator(_, value) {
            if (!value || getFieldValue('password') === value) {
              return Promise.resolve();
            }

            return Promise.reject(new Error('The two passwords that you entered do not match!'));
          }

        })],
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(_components_styled_auth_e__WEBPACK_IMPORTED_MODULE_5__/* .InputPassword */ .C, {})
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Form.Item, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx(antd__WEBPACK_IMPORTED_MODULE_1__.Button, {
          type: "primary",
          htmlType: "submit",
          className: "e-button",
          loading: resetPassword?.isLoading,
          children: "Confirm"
        })
      })]
    })
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PutPassword);
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;