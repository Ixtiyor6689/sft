exports.id = 589;
exports.ids = [589];
exports.modules = {

/***/ 6906:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _banner_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(6148);
/* harmony import */ var _banner_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_banner_module_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);





function Banner(props) {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
    style: {
      backgroundImage: `url("./bd.jpg")`
    },
    className: (_banner_module_scss__WEBPACK_IMPORTED_MODULE_2___default().bannerHelp),
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: (_banner_module_scss__WEBPACK_IMPORTED_MODULE_2___default().drMaker),
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
        className: "pd",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("h1", {
          className: (_banner_module_scss__WEBPACK_IMPORTED_MODULE_2___default().tr),
          children: "Terms & conditions"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
          className: (_banner_module_scss__WEBPACK_IMPORTED_MODULE_2___default().mt),
          children: "Safeeds Transport Inc"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("a", {
          href: "./terms_and_conditions.pdf",
          download: true,
          className: (_banner_module_scss__WEBPACK_IMPORTED_MODULE_2___default().bt),
          children: "Download as PDF"
        })]
      })
    })
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Banner);

/***/ }),

/***/ 4555:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(2681);
/* harmony import */ var _t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);





function TSections(props) {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
    className: `${(_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().tSections)} pd`,
    children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().tx1),
      children: "Thank you for choosing Safeeds Transport Inc LLC (\u201CSafeeds Transport Inc\u201D) for your vehicle\u2019s transport. By using our services, you and the person or entity you represent accept all of the terms and conditions of service (\u201CTERMS\u201D) stated herein. In the event of a conflict between these TERMS and those in any other document, including a motor carrier bill of lading, tariff or service guide, or any other documents, these TERMS will supersede and control as between you and Safeeds Transport Inc. These TERMS cannot be modified by anyone except for Safeeds Transport Inc."
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().tx2),
      children: "Please read Section 12. Arbitration carefully which requires arbitration to first resolve any claim that you may have against Safeeds Transport Inc."
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().title),
      children: "1. Definitions"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("ol", {
      type: "a",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CAdditional Services\u201D"
        }), " means upgrades and additional services ordered by the Customer such as vehicle top-loading, guaranteed pick up date, covered transport, additional personal belongings, etc."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CBill of Lading\u201D"
        }), " means a document provided by the Carrier to the Customer at delivery documenting the details of the Shipment and its condition, Point of Origin, Destination and other details regarding the Order. Bill of Lading can serve as a receipt or a contract between the Carrier and Customer."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CCarrier\u201D"
        }), " means a motor carrier of property, as defined at 49 U.S.C. \xA713102(14), duly licensed by State and/or Federal Department of Transportation, or a carrier of goods by sea pursuant to 46 U.S.C. \xA7 30701."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CCarrier Form\u201D"
        }), " means a receipt, inspection report, Bill of Lading, shipping order or similar document provided by the Carrier at Point of Origin and/or Destination."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CC.O.D.\u201D"
        }), " means collect on delivery or payment on delivery."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CCustomer\u201D"
        }), " means the individual, company or other entity, including its agents and representatives, ordering the transportation of Shipment."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CCustomer\u2019s Agent\u201D"
        }), " means an individual over the age of 18 designated by Customer to act on Customer\u2019s behalf or as an agent."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CDestination\u201D"
        }), " means the Shipment drop off location designated by the Customer or as later modified by mutual agreement between Safeeds Transport Inc and Customer prior to delivery."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CInoperable\u201D"
        }), " means a state or condition in which a Shipment cannot function or be driven for any reason including but not limited to its parts having been removed, altered, damaged or deteriorated such parts including but not limited to engine, transmission, wheels, steering mechanism, brakes, tires, etc."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CSafeeds Transport Inc\u201D"
        }), " means Safeeds Transport Inc LLC, its affiliates and subsidiaries. Safeeds Transport Inc is a transportation broker as defined at 49 U.S.C. \xA7 13102(2), arranging for the transportation of freight through third party Carriers and is duly licensed by the Department of Transportation (DOT), and is registered with the Federal Motor Carrier Safety Administration (FMCSA) under Docket No. MC-01462168 and/or other government agencies as may be required by law. Safeeds Transport Inc is not a Carrier."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201COrder\u201D"
        }), " means Customer\u2019s request for Safeeds Transport Inc to arrange for the transportation of Customer\u2019s Shipment."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201COrder Confirmation\u201D"
        }), " means any written confirmation from Safeeds Transport Inc to the Customer confirming the Customer\u2019s Order and other details including but not limited to description of Shipment, Point of Origin, Destination, dates and quoted rate."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CPoint of Origin\u201D"
        }), " means the Shipment pick up location designated by Customer or as later modified by mutual agreement between Safeeds Transport Inc and Customer prior to transport."]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("li", {
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("b", {
          className: "is-bold",
          children: "\u201CShipment\u201D"
        }), " means the Customer\u2019s property \u2014 an automobile or motorized vehicle \u2014 arranged for transportation in accordance with these TERMS."]
      })]
    }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
      className: "title",
      children: "2. Terms and Conditions"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("ol", {
      type: "a",
      className: "is-with-zero-padding-vertically is-with-zero-margin-vertically",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
        children: "Thank you again for choosing Safeeds Transport Inc for your vehicle shipment.By using our services, you accept all of the terms and conditions of service stated herein. In the event of a conflict between these terms and those in any other document, this will be supersede and control as between you and Safeeds Transport. These Terms cannot be modified by anyone except for Safeeds Transport Inc."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
        className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().te3),
        children: "1. Company Responsibilities"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Upon Customer\u2019s request, Safeeds Transport Inc will arrange for the transportation of Customer\u2019s Shipment by Carrier subject to these TERMS. Company reserves the right, in its sole discretion, to refuse or cancel any order at any time."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Customer understands and accepts that Safeeds Transport Inc operates only as a transportation broker, not a motor carrier or transporter."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Safeeds Transport Inc shall provide customer with an estimated pick up and estimated delivery date, however, doesn\u2019t guarantee on a specified time. Delays may occur prior to, and/or during the transport due to whether or road conditions, government regulations, mechanical problems, and other causes that are beyond company\u2019s control. Customer understands and accepts that Safeeds Transport is not responsible to pay for your rental of a vehicle, nor shall it be liable for failure of mechanical or operating parts of your vehicle."
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("ol", {
      type: "a",
      className: "is-with-zero-padding-vertically is-with-zero-margin-vertically",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
        className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().te3),
        children: "2. Customer Responsibilities"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Personal Property. Customer may leave personal items in the trunk which shall not exceed two hundred pounds (200 lbs) and must be confined to the trunk or storage area of the shipment. All personal items in the vehicle are places there at the owner\u2019s risk, they are not covered by the truck\u2019s insurance. Customer is advised not to leave any negotiable instruments, legal papers, jewelry, furs, money, cash or currency, or any valuable articles in the shipment."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Prohibited Items. Customer understands and accepts that Customer is expressly prohibited from loading any explosives, guns, ammunition, weapons, flammable products, live pets, live plants, any contraband, drugs or narcotics, alcoholic beverages, and or any illegal goods in the Shipment. Customer understands and accepts that upon discovery, such prohibited items and/or the Shipment may be confiscated or disposed of by law enforcement, or the Carrier and the Order may be cancelled in entirety without any remuneration or compensation to Customer and Customer will be solely responsible for any fees, fines, damages, or other liabilities arising from a violation of this Section."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "If the carrier is unable to access the point of origin or destination, Customer agrees to meet the carrier at an alternate location in order for the carrier to safely pick up or drop off the shipment."
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("ol", {
      type: "a",
      className: "is-with-zero-padding-vertically is-with-zero-margin-vertically",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
        className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().te3),
        children: "3. Fees, Payment and Cancellation"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Customer agrees to pay all amounts due in full for each Order and any Additional Services as per the terms of the Order Confirmation and these TERMS without any offsets, chargebacks or reductions by Customer for any actual, pending or unfiled claims, losses, delays, or damages. Payment for STI\u2019s services is due when a Carrier accepts an Order as STI\u2019s services have been rendered at that point."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "All payments for the balance due to Carrier for C.O.D. must be made on or before the delivery of Shipment in the form of cash, certified funds, cashier\u2019s check or money order made payable to the Carrier. Customer WILL NOT use personal checks, debit or credit card when making payments to the Carrier."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Customer may cancel an Order at any time at no cost or cancellation fees as long as the Order has not yet been accepted by a Carrier. If the order is canceled for any reason after a carrier accepts the Order, such reasons including but not limited to Customer canceling an Order, STI canceling an Order due to the Customer\u2019s breach of these TERMS or if the Carrier is denied pick up of the Shipment for any reason when the Carrier arrives at the Point of Origin, then the Customer agrees to pay a minimum of two hundred dollars ($200) in cancellation fees as STI\u2019s services have been rendered at that point."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Cancellation of an Order by Customer must be submitted in writing via email to support@safeeds.us . Cancellations made via telephone, text, chat or any other medium will not be accepted by Safeeds Transport Inc."
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("ol", {
      type: "a",
      className: "is-with-zero-padding-vertically is-with-zero-margin-vertically",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
        className: (_t_sections_module_scss__WEBPACK_IMPORTED_MODULE_2___default().te3),
        children: "4. Loss, Damage and Delay Claims"
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Safeeds Transport Inc is a property transportation broker, therefore, is not and will not be liable for any cargo loss and damage claims for any reason."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Trucking damage claims are covered by carrier from $100,000 up to $250,000 cargo insurance per load, and a minimum of 3/4 of a million dollars public liability and property damage. If Customer has a claim for loss or damage to a Shipment, then Customer understands and agrees that the party liable for all such claims is the Carrier and not STI, and it is Customer\u2019s responsibility to file any claim directly with the Carrier who transported the Shipment at the time of delivery."
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("li", {
        children: "Customer is hereby informed and understands that claims against motor Carriers are governed by federal law, the Carmack Amendment to the ICC Termination Act of 1995, 49 U.S.C. \xA714706, and claims against ocean Carriers are typically governed by the Carriage of Goods by Sea Act, 46 U.S.C. \xA730701. Customer is urged to seek independent legal advice (at Customer\u2019s sole expense) on these laws in the event of a claim."
      })]
    })]
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TSections);

/***/ }),

/***/ 6148:
/***/ ((module) => {

// Exports
module.exports = {
	"bannerHelp": "banner_bannerHelp___6t7s",
	"drMaker": "banner_drMaker__JW4_0",
	"tr": "banner_tr__TgdVP",
	"mt": "banner_mt__fhH6f",
	"bt": "banner_bt__jstOR"
};


/***/ }),

/***/ 2681:
/***/ ((module) => {

// Exports
module.exports = {
	"tSections": "t-sections_tSections__RlGFA",
	"tx1": "t-sections_tx1__XaNdc",
	"tx2": "t-sections_tx2__rPJzX",
	"title": "t-sections_title__Zrl49",
	"te3": "t-sections_te3__PAI5z"
};


/***/ }),

/***/ 9331:
/***/ ((module) => {

// Exports
module.exports = {
	"help": "help_help__MPT7s"
};


/***/ })

};
;