"use strict";
exports.id = 67;
exports.ids = [67];
exports.modules = {

/***/ 1727:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "d": () => (/* binding */ ResendLink)
/* harmony export */ });
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(7518);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_0__);

const ResendLink = styled_components__WEBPACK_IMPORTED_MODULE_0___default().span.withConfig({
  displayName: "check-emaile__ResendLink",
  componentId: "sc-1djucq8-0"
})(["color:#1890ff;cursor:pointer;transition:color 0.3s;margin-left:15px;&:hover{color:#40a9ff;}"]);

/***/ }),

/***/ 8964:
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Z": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__);



const UseCountDown = ({
  timer
}) => {
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("div", {
    className: "App",
    children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx("h2", {
      children: timer
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (UseCountDown);

/***/ }),

/***/ 5067:
/***/ ((module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(6689);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_code_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(6339);
/* harmony import */ var react_code_input__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_code_input__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(5725);
/* harmony import */ var antd__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var hooks__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(8102);
/* harmony import */ var react_toastify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(3590);
/* harmony import */ var _components_styled_check_email_e__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(1727);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(6022);
/* harmony import */ var react_redux__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(react_redux__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _hooks_useCountDown__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(8964);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(997);
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__);
var __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([hooks__WEBPACK_IMPORTED_MODULE_3__, react_toastify__WEBPACK_IMPORTED_MODULE_4__]);
([hooks__WEBPACK_IMPORTED_MODULE_3__, react_toastify__WEBPACK_IMPORTED_MODULE_4__] = __webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }












function CheckEmail({
  setStep,
  email
}) {
  const dispatch = (0,react_redux__WEBPACK_IMPORTED_MODULE_6__.useDispatch)();
  const {
    0: timer,
    1: setTimer
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)('00:00');
  const Ref = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)(null);
  const props = {
    className: "reactCodeInput",
    inputStyle: {
      fontFamily: "Poppins",
      margin: "10px",
      width: "60px",
      minHeight: "60px",
      borderRadius: "3px",
      fontSize: "20px",
      backgroundColor: "",
      textAlign: "center",
      color: "lightskyblue",
      border: "2px solid lightskyblue"
    }
  };
  const sendCode = (0,hooks__WEBPACK_IMPORTED_MODULE_3__/* .useSendCode */ .At)({
    onSuccess() {
      react_toastify__WEBPACK_IMPORTED_MODULE_4__.toast.success("We have sent sms");
    },

    onError(err) {
      react_toastify__WEBPACK_IMPORTED_MODULE_4__.toast.error(err?.data?.error);
    }

  });
  const checkCode = (0,hooks__WEBPACK_IMPORTED_MODULE_3__/* .useCheckCode */ .kD)({
    onSuccess() {
      react_toastify__WEBPACK_IMPORTED_MODULE_4__.toast.success("Successfully checked");
      setStep("put_password");
    },

    onError(err) {
      react_toastify__WEBPACK_IMPORTED_MODULE_4__.toast.error(err?.data?.error);
    }

  });

  const onFinish = values => {
    checkCode.mutate({
      data: {
        email: email,
        code: Number(values?.code)
      },
      dispatch: dispatch
    });
  };

  const onFinishFailed = errorInfo => {
    console.log('Failed:', errorInfo);
  };

  const handleResendSms = async () => {
    sendCode.mutate({
      email: email
    });
    clearTimer(getDeadTime());
  };

  const getTimeRemaining = e => {
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor(total / 1000 % 60);
    const minutes = Math.floor(total / 1000 / 60 % 60);
    const hours = Math.floor(total / 1000 / 60 / 60 % 24);
    return {
      total,
      hours,
      minutes,
      seconds
    };
  };

  const startTimer = e => {
    let {
      total,
      minutes,
      seconds
    } = getTimeRemaining(e);

    if (total >= 0) {
      // update the timer
      // check if less than 10 then we need to
      // add '0' at the beginning of the variable
      setTimer((minutes > 9 ? minutes : '0' + minutes) + ':' + (seconds > 9 ? seconds : '0' + seconds));
    }
  };

  const clearTimer = e => {
    setTimer('02:00');
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  const getDeadTime = () => {
    let deadline = new Date();
    deadline.setSeconds(deadline.getSeconds() + 120);
    return deadline;
  };

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    clearTimer(getDeadTime());
  }, []);
  return /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx("div", {
    className: "cel",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsxs)(antd__WEBPACK_IMPORTED_MODULE_2__.Form, {
      name: "basic",
      initialValues: {
        remember: true
      },
      onFinish: onFinish,
      onFinishFailed: onFinishFailed,
      autoComplete: "off",
      layout: "vertical",
      className: "form",
      children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx(antd__WEBPACK_IMPORTED_MODULE_2__.Form.Item, {
        label: "Code",
        name: "code",
        style: {
          marginBottom: 0
        },
        rules: [{
          required: true,
          message: 'Please input your email!'
        }],
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx((react_code_input__WEBPACK_IMPORTED_MODULE_1___default()), _objectSpread({
          inputMode: "numeric",
          name: "smsCode",
          fields: 4
        }, props))
      }), timer === "00:00" ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsxs)("div", {
        className: "lres",
        children: [/*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx("span", {
          children: "Don't get SMS?"
        }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx(_components_styled_check_email_e__WEBPACK_IMPORTED_MODULE_5__/* .ResendLink */ .d, {
          onClick: () => {
            handleResendSms();
          },
          children: "Resend"
        })]
      }) : /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx(_hooks_useCountDown__WEBPACK_IMPORTED_MODULE_7__/* ["default"] */ .Z, {
        setTimer: setTimer,
        timer: timer
      }), /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx(antd__WEBPACK_IMPORTED_MODULE_2__.Form.Item, {
        children: /*#__PURE__*/react_jsx_runtime__WEBPACK_IMPORTED_MODULE_8__.jsx(antd__WEBPACK_IMPORTED_MODULE_2__.Button, {
          type: "primary",
          htmlType: "submit",
          className: "e-button",
          loading: checkCode?.isLoading,
          children: "Confirm"
        })
      })]
    })
  });
}

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CheckEmail);
__webpack_async_result__();
} catch(e) { __webpack_async_result__(e); } });

/***/ })

};
;